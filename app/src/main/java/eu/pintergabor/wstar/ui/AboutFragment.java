package eu.pintergabor.wstar.ui;

import android.os.Bundle;

import androidx.annotation.*;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import eu.pintergabor.wstar.BuildConfig;
import eu.pintergabor.wstar.R;

/**
 * About.
 */
public class AboutFragment extends Fragment {

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public View onCreateView(
        @NonNull LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_about, container, false);
        // Set version number
        final TextView version = v.findViewById(R.id.about_version);
        String versionText;
        if (BuildConfig.BUILD_TYPE.equals("debug")) {
            versionText = String.format("%s (%s)",
                BuildConfig.VERSION_NAME,
                BuildConfig.FLAVOR);
        } else {
            versionText = BuildConfig.VERSION_NAME;
        }
        version.setText(versionText);
        // Done
        return v;
    }

}
