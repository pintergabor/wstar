package eu.pintergabor.wstar.ui.helper;

import eu.pintergabor.wstar.ui.MainActivity;

/**
 * Manage views displayed in the main application frame.
 */
public final class ManageViews extends ManageViewsBase {

    // region // Constructor.

    public ManageViews(MainActivity mainActivity) {
        super(mainActivity);
    }

    // endregion

    // region // View states.

    /**
     * All possible view states.
     *
     * <table>
     * <tr><td>MAIN</td><td> - </td><td>Main only</td></tr>
     * <tr><td>SETTINGS</td><td> - </td><td>Settings only</td></tr>
     * <tr><td>ABOUT</td><td> - </td><td>About only</td></tr>
     * <tr><td>SETTINGS_MAIN</td><td> - </td><td>Settings and main</td></tr>
     * <tr><td>ABOUT_MAIN</td><td> - </td><td>Main and about</td></tr>
     * </table>
     * <p>
     * In portrait, or near portrait mode only SETTINGS, MAIN and ABOUT are used.
     */
    public enum VIEWSTATE {
        MAIN,
        SETTINGS,
        ABOUT,
        SETTINGS_MAIN,
        ABOUT_MAIN,
    }

    private VIEWSTATE currentViewstate = VIEWSTATE.MAIN;

    /**
     * Current view state.
     *
     * @return Current {@link VIEWSTATE}.
     */
    @SuppressWarnings("unused")
    public VIEWSTATE getCurrentViewstate() {
        return currentViewstate;
    }

    /**
     * Change view state.
     *
     * @param v New {@link VIEWSTATE}.
     */
    @SuppressWarnings("unused")
    public void setCurrentViewstate(VIEWSTATE v) {
        if (v != null) {
            // SETTINGS_MAIN and ABOUT_MAIN states are invalid in portrait mode.
            if (portrait() &&
                (v == VIEWSTATE.SETTINGS_MAIN || v == VIEWSTATE.ABOUT_MAIN)) {
                v = VIEWSTATE.MAIN;
            }
            // Transition
            mainActivity.getTransitions().aniViewstate(v);
            // Remember the view state.
            currentViewstate = v;
        }
    }

    /**
     * Refresh current view state, if frame size or orientation changes.
     */
    @SuppressWarnings("unused")
    @Override
    public void refreshCurrentViewstate() {
        setCurrentViewstate(currentViewstate);
    }

    // endregion

}
