package eu.pintergabor.wstar.ui.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import androidx.preference.PreferenceManager;

import java.util.Map;

import eu.pintergabor.wstar.BuildConfig;
import eu.pintergabor.wstar.R;

/**
 * Set default values on first run.
 */
public final class Defaults {
    /**
     * Static class.
     */
    private Defaults() {
        // Nothing
    }

    /**
     * Set default values on first run.
     *
     * @param context Some context.
     */
    @SuppressWarnings("ConstantConditions")
    public static void setDefaults(Context context) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final Map<String, ?> spmap = prefs.getAll();
        // Create defaults on first run
        if (spmap.get("drawing_type") == null) {
            final SharedPreferences.Editor prefed = prefs.edit();
            String defaulttype;
            int defaultpoints, defaultn, defaultm;
            final Resources r = context.getResources();
            switch (BuildConfig.FLAVOR) {
                default:
                    defaulttype = "wstar";
                    defaultpoints = r.getInteger(R.integer.wstar_default_points);
                    defaultn = r.getInteger(R.integer.wstar_default_n);
                    defaultm = r.getInteger(R.integer.wstar_default_m);
                    break;
                case "pascal":
                    defaulttype = "pascal";
                    defaultpoints = r.getInteger(R.integer.pascal_default_points);
                    defaultn = r.getInteger(R.integer.pascal_default_n);
                    defaultm = r.getInteger(R.integer.pascal_default_m);
                    break;
                case "nephroid":
                    defaulttype = "nephroid";
                    defaultpoints = r.getInteger(R.integer.nephroid_default_points);
                    defaultn = r.getInteger(R.integer.nephroid_default_n);
                    defaultm = r.getInteger(R.integer.nephroid_default_m);
                    break;
            }
            // App
            prefed.putString("drawing_type", defaulttype);
            prefed.putInt("points", defaultpoints);
            prefed.putInt("line_width", r.getInteger(R.integer.default_line_width));
            prefed.putInt("param_n", defaultn);
            prefed.putInt("param_m", defaultm);
            prefed.putInt("background_red", r.getInteger(R.integer.default_background_red));
            prefed.putInt("background_green", r.getInteger(R.integer.default_background_green));
            prefed.putInt("background_blue", r.getInteger(R.integer.default_background_blue));
            prefed.putInt("foreground_red", r.getInteger(R.integer.default_foreground_red));
            prefed.putInt("foreground_green", r.getInteger(R.integer.default_foreground_green));
            prefed.putInt("foreground_blue", r.getInteger(R.integer.default_foreground_blue));
            // Wallpaper
            prefed.putString("wallpaper_drawing_type", defaulttype);
            prefed.putInt("wallpaper_points", defaultpoints);
            prefed.putInt("wallpaper_line_width", r.getInteger(R.integer.default_line_width));
            prefed.putInt("wallpaper_param_n", defaultn);
            prefed.putInt("wallpaper_param_m", defaultm);
            prefed.putInt("wallpaper_background_red", r.getInteger(R.integer.wallpaper_default_background_red));
            prefed.putInt("wallpaper_background_green", r.getInteger(R.integer.wallpaper_default_background_green));
            prefed.putInt("wallpaper_background_blue", r.getInteger(R.integer.wallpaper_default_background_blue));
            prefed.putInt("wallpaper_foreground_red", r.getInteger(R.integer.wallpaper_default_foreground_red));
            prefed.putInt("wallpaper_foreground_green", r.getInteger(R.integer.wallpaper_default_foreground_green));
            prefed.putInt("wallpaper_foreground_blue", r.getInteger(R.integer.wallpaper_default_foreground_blue));
            prefed.apply();
        }

    }
}
