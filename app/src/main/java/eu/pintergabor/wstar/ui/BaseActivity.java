package eu.pintergabor.wstar.ui;

import android.os.Bundle;

import androidx.annotation.*;
import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.*;

import eu.pintergabor.util.runcount.RunCount;
import eu.pintergabor.wstar.main.MainHub;

/**
 * Main program
 */
public abstract class BaseActivity extends AppCompatActivity {

    // region // "Globals".

    private MainHub mainHub;

    /**
     * Communication interface.
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    @NonNull
    public MainHub getMainHub() {
        return mainHub;
    }

    // endregion

    // region // Lifecycle events.

    /**
     * Start of the program.
     *
     * @param bundle Previous state.
     */
    @Override
    protected void onCreate(Bundle bundle) {
        // Restore state
        super.onCreate(bundle);
        // Load, and increment, run counter
        RunCount.getInstance().load(this).increment();
        // Create communication interface
        mainHub = new MainHub();
    }

    /**
     * Save state.
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle bundle) {
        super.onSaveInstanceState(bundle);
        // Save the already incremented run counter
        RunCount.getInstance().save(this);
    }

    // endregion

}
