package eu.pintergabor.wstar.ui.helper;

import android.content.SharedPreferences;
import android.content.res.Resources;

import androidx.annotation.*;
import androidx.preference.Preference;
import androidx.preference.PreferenceManager;

import eu.pintergabor.util.basic.Cast;
import eu.pintergabor.util.preferences.ValueToSummary;
import eu.pintergabor.wstar.BuildConfig;
import eu.pintergabor.wstar.R;
import eu.pintergabor.wstar.main.MainHub;
import eu.pintergabor.wstar.ui.BaseActivity;
import eu.pintergabor.wstar.ui.MainActivity;
import eu.pintergabor.wstar.ui.PrefsFragment;

/**
 * Help {@link PrefsFragment}.
 */
public final class PrefsChangeListener implements Preference.OnPreferenceChangeListener {

    /**
     * The  helped {@link PrefsFragment}.
     */
    private final PrefsFragment helped;

    /**
     * Help {@link PrefsFragment}.
     *
     * @param fragment The  helped {@link PrefsFragment}.
     */
    public PrefsChangeListener(@NonNull PrefsFragment fragment) {
        helped = fragment;
    }

    /**
     * Get {@link MainHub}.
     *
     * @return The only {@link MainHub} instance from {@link MainActivity}.
     */
    @NonNull
    private MainHub getMainHub() {
        return ((BaseActivity) helped.requireActivity()).getMainHub();
    }

    /**
     * Lock drawing type in certain flavors.
     */
    @SuppressWarnings("ConstantConditions")
    public void lockDrawingType() {
        final Resources r = helped.getResources();
        switch (BuildConfig.FLAVOR) {
            case "wstar":
                helped.drawingType.setValue("wstar");
                helped.drawingType.setVisible(false);
                break;
            case "pascal":
                helped.drawingType.setValue("pascal");
                helped.drawingType.setVisible(false);
                break;
            case "nephroid":
                helped.drawingType.setValue("nephroid");
                helped.drawingType.setVisible(false);
                break;
        }
    }

    /**
     * Adjust the visibility and range of preference items.
     *
     * @param type Drawing type.
     */
    public void adjustPrefs(String type) {
        final MainHub mainHub = getMainHub();
        final Resources r = helped.getResources();
        int maxpoints, maxn, maxm;
        int defaultpoints, defaultn, defaultm;
        switch (type) {
            default: // === case "wstar":
                maxpoints = r.getInteger(R.integer.wstar_max_points);
                maxn = r.getInteger(R.integer.wstar_max_n);
                maxm = r.getInteger(R.integer.wstar_max_m);
                defaultpoints = r.getInteger(R.integer.wstar_default_points);
                defaultn = r.getInteger(R.integer.wstar_default_n);
                defaultm = r.getInteger(R.integer.wstar_default_m);
                break;
            case "pascal":
                maxpoints = r.getInteger(R.integer.pascal_max_points);
                maxn = r.getInteger(R.integer.pascal_max_n);
                maxm = r.getInteger(R.integer.pascal_max_m);
                defaultpoints = r.getInteger(R.integer.pascal_default_points);
                defaultn = r.getInteger(R.integer.pascal_default_n);
                defaultm = r.getInteger(R.integer.pascal_default_m);
                break;
            case "nephroid":
                maxpoints = r.getInteger(R.integer.nephroid_max_points);
                maxn = r.getInteger(R.integer.nephroid_max_n);
                maxm = r.getInteger(R.integer.nephroid_max_m);
                defaultpoints = r.getInteger(R.integer.nephroid_default_points);
                defaultn = r.getInteger(R.integer.nephroid_default_n);
                defaultm = r.getInteger(R.integer.nephroid_default_m);
                break;
        }
        // Max points
        if (maxpoints < helped.points.getValue()) {
            helped.points.setValue(defaultpoints);
            mainHub.setPoints(defaultpoints);
        }
        helped.points.setMax(maxpoints);
        // Max N
        if (maxn < 0) {
            helped.paramN.setVisible(false);
        } else {
            helped.paramN.setVisible(true);
            if (maxn < helped.paramN.getValue()) {
                helped.paramN.setValue(defaultn);
                mainHub.setParamN(defaultn);
            }
            helped.paramN.setMax(maxn);
        }
        // Max M
        if (maxm < 0) {
            helped.paramM.setVisible(false);
        } else {
            helped.paramM.setVisible(true);
            if (maxm < helped.paramM.getValue()) {
                helped.paramM.setValue(defaultm);
                mainHub.setParamM(defaultm);
            }
            helped.paramM.setMax(maxm);
        }
        // Request redraw
        mainHub.redrawRequest();
    }

    /**
     * Called when a preference changes that might have effects on the main drawing.
     * <p/>
     * It should be private, because it is used only in {@link #tieChange(Preference)}.
     *
     * @return Always true = Always allow updating the preference to the new value.
     */
    @Override
    public boolean onPreferenceChange(Preference preference, Object value) {
        final MainHub mainHub = getMainHub();
        if (preference == helped.drawingType) {
            // Update summary
            ValueToSummary.updateSummary(preference, value);
            // Adjust the visibility and range of preference items
            adjustPrefs(Cast.ObjectToString(value));
            // Change drawing
            mainHub.setDrawingType(Cast.ObjectToString(value));
        } else if (preference == helped.points) {
            mainHub.setPoints(Cast.ObjectToInt(value));
        } else if (preference == helped.paramN) {
            mainHub.setParamN(Cast.ObjectToInt(value));
        } else if (preference == helped.paramM) {
            mainHub.setParamM(Cast.ObjectToInt(value));
        } else if (preference == helped.lineWidth) {
            mainHub.setLineWidth(Cast.ObjectToFloat(value));
        } else if (preference == helped.backgroundRed) {
            mainHub.setBackRed(Cast.ObjectToInt(value));
        } else if (preference == helped.backgroundGreen) {
            mainHub.setBackGreen(Cast.ObjectToInt(value));
        } else if (preference == helped.backgroundBlue) {
            mainHub.setBackBlue(Cast.ObjectToInt(value));
        } else if (preference == helped.foregroundRed) {
            mainHub.setForeRed(Cast.ObjectToInt(value));
        } else if (preference == helped.foregroundGreen) {
            mainHub.setForeGreen(Cast.ObjectToInt(value));
        } else if (preference == helped.foregroundBlue) {
            mainHub.setForeBlue(Cast.ObjectToInt(value));
        }
        // Request redraw
        mainHub.redrawRequest();
        return true;
    }

    /**
     * Tie preference changes to actions.
     *
     * @param preference Some settings that might have effect on the main drawing.
     */
    public void tieChange(@NonNull Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(this);
        // Trigger the listener immediately with the current value of the preference.
        SharedPreferences sp = PreferenceManager.
            getDefaultSharedPreferences(preference.getContext());
        Object value = sp.getAll().get(preference.getKey());
        onPreferenceChange(preference, value);
    }

}
