package eu.pintergabor.wstar.ui.helper;

import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import androidx.core.view.GestureDetectorCompat;

import eu.pintergabor.util.ui.ScrollDetector;
import eu.pintergabor.util.ui.ZoomDetector;
import eu.pintergabor.wstar.main.MainHub;
import eu.pintergabor.wstar.ui.MainActivity;

/**
 * Help {@link MainActivity} with handling touches.
 */
public final class Touch {

    // region // Listeners.

    /**
     * Used by {@link #scrollDetector}.
     */
    private GestureDetectorCompat gestureDetector;

    /**
     * Detect scrolls.
     */
    private ScrollDetector scrollDetector;

    /**
     * Used by {@link #zoomDetector}.
     */
    private ScaleGestureDetector scaleGestureDetector;

    /**
     * Detect two finger gestures, like pan and scale.
     */
    private ZoomDetector zoomDetector;

    /**
     * Called when a scroll gesture ends.
     */
    public ScrollDetector.ScrollListener scrollListener = new ScrollDetector.ScrollListener() {
        @Override
        public void onScroll(float startX, float startY,
                             float distanceX, float distanceY) {
            final ManageViews manageViews = mainActivity.getManageViews();
            final float w = manageViews.getFrameWidth();
            final float h = manageViews.getFrameHeight();
            final MainHub mainHub = mainActivity.getMainHub();
            mainHub.onScroll(distanceX / w, distanceY / h);
        }
    };

    /**
     * Called when a two finger gesture ends.
     */
    public ZoomDetector.ZoomListener zoomListener = new ZoomDetector.ZoomListener() {
        @Override
        public void onZoom(float startX, float startY, float scale) {
            final ManageViews manageViews = mainActivity.getManageViews();
            final float w = manageViews.getFrameWidth();
            final float h = manageViews.getFrameHeight();
            final MainHub mainHub = mainActivity.getMainHub();
            mainHub.onZoom(startX / w, startY / h, scale);
        }
    };

    // endregion

    // region // Constructor and link to MainActivity.

    /**
     * Link to {@link MainActivity}.
     */
    private final MainActivity mainActivity;

    /**
     * Start listening.
     */
    public Touch(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        // Scrolls
        scrollDetector = new ScrollDetector();
        scrollDetector.setScrollListener(scrollListener);
        gestureDetector = new GestureDetectorCompat(mainActivity, scrollDetector);
        // Zoom
        zoomDetector = new ZoomDetector();
        zoomDetector.setZoomListener(zoomListener);
        scaleGestureDetector = new ScaleGestureDetector(mainActivity, zoomDetector);
    }

    // endregion

    // region // Hook into touch events.

    /**
     * Hook {@link #gestureDetector} and
     * {@link #scaleGestureDetector} into touch events.
     */
    public void onTouchEvent(MotionEvent event) {
        if (gestureDetector != null) {
            gestureDetector.onTouchEvent(event);
        }
        if (scaleGestureDetector != null) {
            scaleGestureDetector.onTouchEvent(event);
        }
    }

    // endregion

}
