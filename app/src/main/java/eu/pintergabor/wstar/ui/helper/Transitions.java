package eu.pintergabor.wstar.ui.helper;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import eu.pintergabor.util.ui.Util;
import eu.pintergabor.wstar.R;
import eu.pintergabor.wstar.ui.MainActivity;

public class Transitions {

    // region // Constructor and link to MainActivity.

    /**
     * Link to {@link MainActivity}.
     */
    private final MainActivity mainActivity;


    /**
     * Nothing.
     */
    public Transitions(@NonNull MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    // endregion

    /**
     * Transition slide time.
     */
    private static final int SLIDETIME = 300;

    /**
     * Slide out overlay view to left.
     *
     * @param view  The view to slide out.
     * @param width The width of the view.
     */
    private void slideOut(View view, int width) {
        // Initial state: the view is visible, inside, on the left side
        Util.setOffset(view, 0);
        view.setVisibility(View.VISIBLE);
        // Transition
        ValueAnimator animator = ValueAnimator.ofInt(0, width);
        animator.setDuration(SLIDETIME);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int v = (int) animation.getAnimatedValue();
                Util.setOffset(view, -v);
            }
        });
        animator.addListener(new AnimatorListenerAdapter() {
            /**
             * Hide the view, when it is completely outside.
             */
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
            }
        });
        animator.start();
    }

    /**
     * Slide in overlay view from left.
     *
     * @param view     The view to slide in.
     * @param width    The width of the view.
     * @param hideview The view to hide at the end of the transition.
     *                 It could be null.
     */
    private void slideIn(View view, int width, @Nullable View hideview) {
        // Initial state: the view will be visible, when it slides in,
        // but now it is outside, on the left side.
        Util.setOffset(view, -width);
        view.setVisibility(View.VISIBLE);
        // Transition
        ValueAnimator animator = ValueAnimator.ofInt(0, width);
        animator.setDuration(SLIDETIME);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int v = (int) animation.getAnimatedValue();
                Util.setOffset(view, v - width);
            }
        });
        if (hideview != null) {
            animator.addListener(new AnimatorListenerAdapter() {
                /**
                 * Hide the view that is completely covered with the view that slided in.
                 */
                @Override
                public void onAnimationEnd(Animator animation) {
                    hideview.setVisibility(View.GONE);
                }
            });
        }
        animator.start();
    }

    /**
     * Set view state without animation.
     *
     * @param v New state
     */
    public void setViewstate(ManageViews.VIEWSTATE v) {
        if (v != null) {
            final ManageViews manageViews = mainActivity.getManageViews();
            // Get references to the embedded views
            final View main = mainActivity.findViewById(R.id.fragment_main);
            final View settings = mainActivity.findViewById(R.id.fragment_settings);
            final View about = mainActivity.findViewById(R.id.fragment_about);
            // Change the size of the embedded views.
            final int w = manageViews.getFrameWidth();
            final int h = manageViews.getFrameHeight();
            final int r = w - h;
            final boolean portrait = manageViews.portrait();
            switch (v) {
                default: // === case MAIN:
                    if (portrait) {
                        Util.setSize(settings, w, h);
                        Util.setOffset(settings, -w);
                        Util.setSize(about, w, h);
                        Util.setOffset(about, -w);
                    } else {
                        Util.setSize(settings, r, h);
                        Util.setOffset(settings, -r);
                        Util.setSize(about, r, h);
                        Util.setOffset(about, -r);
                    }
                    settings.setVisibility(View.GONE);
                    about.setVisibility(View.GONE);
                    main.setVisibility(View.VISIBLE);
                    main.requestFocus();
                    break;
                case SETTINGS:
                    Util.setSize(settings, w, h);
                    Util.setOffset(settings, 0);
                    settings.setVisibility(View.VISIBLE);
                    about.setVisibility(View.GONE);
                    main.setVisibility(View.GONE);
                    settings.requestFocus();
                    break;
                case ABOUT:
                    Util.setSize(about, w, h);
                    Util.setOffset(about, 0);
                    settings.setVisibility(View.GONE);
                    about.setVisibility(View.VISIBLE);
                    main.setVisibility(View.GONE);
                    about.requestFocus();
                    break;
                case SETTINGS_MAIN:
                    Util.setSize(settings, r, h);
                    Util.setOffset(settings, 0);
                    settings.setVisibility(View.VISIBLE);
                    about.setVisibility(View.GONE);
                    main.setVisibility(View.VISIBLE);
                    main.requestFocus();
                    break;
                case ABOUT_MAIN:
                    Util.setSize(about, r, h);
                    Util.setOffset(about, 0);
                    settings.setVisibility(View.GONE);
                    about.setVisibility(View.VISIBLE);
                    main.setVisibility(View.VISIBLE);
                    main.requestFocus();
                    break;
            }
        }
    }

    /**
     * Set view state without animation.
     *
     * @param v New state
     */
    public void aniViewstate(ManageViews.VIEWSTATE v) {
        if (v != null) {
            final ManageViews manageViews = mainActivity.getManageViews();
            // Get references to the embedded views
            final View main = mainActivity.findViewById(R.id.fragment_main);
            final View settings = mainActivity.findViewById(R.id.fragment_settings);
            final View about = mainActivity.findViewById(R.id.fragment_about);
            // Change the size of the embedded views.
            final int w = manageViews.getFrameWidth();
            final int h = manageViews.getFrameHeight();
            final int r = w - h;
            final ManageViews.VIEWSTATE cvs = manageViews.getCurrentViewstate();
            // Set current state again to make sure this is the starting point
            setViewstate(cvs);
            // Transition to new state
            switch (v) {
                case MAIN:
                    switch (cvs) {
                        case SETTINGS:
                            slideOut(settings, w);
                            break;
                        case ABOUT:
                            slideOut(about, w);
                            break;
                        case SETTINGS_MAIN:
                            slideOut(settings, r);
                            break;
                        case ABOUT_MAIN:
                            slideOut(about, r);
                            break;
                    }
                    main.setVisibility(View.VISIBLE);
                    main.requestFocus();
                    break;
                case SETTINGS:
                    switch (cvs) {
                        case MAIN:
                        case ABOUT:
                            slideIn(settings, w, main);
                            break;
                        case SETTINGS_MAIN:
                        case ABOUT_MAIN:
                            setViewstate(v);
                            break;
                    }
                    settings.requestFocus();
                    break;
                case ABOUT:
                    switch (cvs) {
                        case MAIN:
                        case SETTINGS:
                            slideIn(about, w, main);
                            break;
                        case SETTINGS_MAIN:
                        case ABOUT_MAIN:
                            setViewstate(v);
                            break;
                    }
                    about.requestFocus();
                    break;
                case SETTINGS_MAIN:
                    switch (cvs) {
                        case MAIN:
                        case ABOUT_MAIN:
                            settings.bringToFront();
                            slideIn(settings, r, null);
                            break;
                        case SETTINGS:
                        case ABOUT:
                            setViewstate(v);
                            break;
                    }
                    settings.requestFocus();
                    break;
                case ABOUT_MAIN:
                    switch (cvs) {
                        case MAIN:
                        case SETTINGS_MAIN:
                            about.bringToFront();
                            slideIn(about, r, null);
                            break;
                        case SETTINGS:
                        case ABOUT:
                            setViewstate(v);
                            break;
                    }
                    about.requestFocus();
                    break;
            }
        }
    }

}
