package eu.pintergabor.wstar.ui;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;

import androidx.annotation.*;

import org.jetbrains.annotations.*;

import eu.pintergabor.wstar.R;
import eu.pintergabor.wstar.file.Save;
import eu.pintergabor.wstar.ui.helper.Buttons;
import eu.pintergabor.wstar.ui.helper.Defaults;
import eu.pintergabor.wstar.ui.helper.ManageViews;
import eu.pintergabor.wstar.ui.helper.Touch;
import eu.pintergabor.wstar.ui.helper.Transitions;

/**
 * Main program
 */
public final class MainActivity extends BaseActivity {

    // region // "Globals".

    private ManageViews manageViews;

    /**
     * Manage the visibility and size of views.
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    @NonNull
    public ManageViews getManageViews() {
        return manageViews;
    }

    /**
     * Handling buttons and keys.
     */
    private Buttons buttons;

    /**
     * Handling touch events.
     */
    private Touch touch;

    private Save save;

    /**
     * Saving drawings.
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    @NonNull
    public Save getSave() {
        return save;
    }

    private Transitions transitions;

    /**
     * Transitions.
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    @NonNull
    public Transitions getTransitions() {
        return transitions;
    }

    // endregion

    // region // Lifecycle events.

    /**
     * Start of the program.
     *
     * @param bundle Previous state.
     */
    @Override
    protected void onCreate(Bundle bundle) {
        // Restore state
        super.onCreate(bundle);
        // Set defaults
        Defaults.setDefaults(this);
        // Transitions
        transitions = new Transitions(this);
        // Main layout
        setContentView(R.layout.activity_main);
        manageViews = new ManageViews(this);
        // Buttons visibility and control handler
        buttons = new Buttons(this);
        // Touch handler
        touch = new Touch(this);
        // Hook Save.onRestoreInstanceState
        save = new Save(this);
        save.onRestoreInstanceState(bundle);
        // Create launcher icon (empty in "release" build)
        //StaticTests.makeLauncherIcon();
    }

    /**
     * Prepare to show main view.
     */
    @Override
    protected void onResume() {
        super.onResume();
        // Restore last view state
        manageViews.refreshCurrentViewstate();
    }

    /**
     * Save state.
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle bundle) {
        super.onSaveInstanceState(bundle);
        // Hook Save.onSaveInstanceState
        save.onSaveInstanceState(bundle);
    }

    // endregion

    // region // Touch, key and permission hooks.

    /**
     * Hook {@link Touch#onTouchEvent(MotionEvent)} into touch events.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Touch detection
        touch.onTouchEvent(event);
        // Show and delay hide buttons
        buttons.delayHide();
        return true;
    }

    /**
     * Hook {@link Buttons#onBackPressed()} into back button processing.
     */
    @Override
    public void onBackPressed() {
        if (!buttons.onBackPressed()) {
            super.onBackPressed();
        }
    }

    /**
     * Hook {@link Buttons#onKeyDown(int, KeyEvent)} into key press processing.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (buttons.onKeyDown(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Hook {@link Save#onRequestPermissionsResult(int, String[], int[])} into permission result handling.
     */
    @Override
    public void onRequestPermissionsResult(
        int requestCode,
        @NonNull String[] permissions,
        @NonNull int[] grantResults) {
        save.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    // endregion

}
