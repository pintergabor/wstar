package eu.pintergabor.wstar.ui.helper;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.*;
import androidx.fragment.app.DialogFragment;

import eu.pintergabor.wstar.R;
import eu.pintergabor.wstar.ui.MainActivity;

/**
 * Save WSTAR as a custom size bitmap.
 */
public class SizeDialogFragment extends DialogFragment {

    /**
     * Width box.
     */
    private EditText widthBox;

    /**
     * Height box.
     */
    private EditText heightBox;

    /**
     * Create and display the dialog.
     */
    @Override
    public View onCreateView(
        @NonNull LayoutInflater inflater,
        ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate
        final View v = inflater.inflate(R.layout.dialog_size, container, false);
        // Edit text fields
        widthBox = v.findViewById(R.id.size_width);
        heightBox = v.findViewById(R.id.size_height);
        // Button
        final Button buttonOK = v.findViewById(R.id.size_ok);
        buttonOK.setOnClickListener(actionOK);
        // Return
        return v;
    }

    /**
     * Get the numerical value of an {@link EditText}.
     *
     * @return The value, or 0 if the box does not contain a number.
     */
    @SuppressWarnings("ConstantConditions")
    private int getValue(EditText box) {
        try {
            return Integer.parseInt(box.getText().toString());
        } catch (NullPointerException | NumberFormatException e) {
            return 0;
        }
    }

    /**
     * Click OK.
     * <p>
     * Get size and save WSTAR as a custom size bitmap.
     */
    private final View.OnClickListener actionOK = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final Dialog dialog = getDialog();
            if (dialog != null) {
                final int width = getValue(widthBox);
                final int height = getValue(heightBox);
                if (0 < width && 0 < height) {
                    final MainActivity mainActivity = (MainActivity) requireActivity();
                    mainActivity.getSave().save(width, height);
                }
                getDialog().dismiss();
            }
        }
    };

}
