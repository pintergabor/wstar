package eu.pintergabor.wstar.ui;

import android.os.Bundle;

import androidx.preference.ListPreference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SeekBarPreference;

import eu.pintergabor.wstar.R;
import eu.pintergabor.wstar.ui.helper.PrefsChangeListener;

/**
 * The core of settings.
 */
public class PrefsFragment extends PreferenceFragmentCompat {

    // region // References.

    /**
     * Persistent reference to "drawing_type" preference.
     */
    public ListPreference drawingType;

    /**
     * Persistent reference to "points" preference.
     */
    public SeekBarPreference points;

    /**
     * Persistent reference to "param_n" preference.
     */
    public SeekBarPreference paramN;

    /**
     * Persistent reference to "param_m" preference.
     */
    public SeekBarPreference paramM;

    /**
     * Persistent reference to "line_width" preference.
     */
    public SeekBarPreference lineWidth;

    /**
     * Persistent reference to "background_red" preference.
     */
    public SeekBarPreference backgroundRed;

    /**
     * Persistent reference to "background_green" preference.
     */
    public SeekBarPreference backgroundGreen;

    /**
     * Persistent reference to "background_blue" preference.
     */
    public SeekBarPreference backgroundBlue;

    /**
     * Persistent reference to "foreground_red" preference.
     */
    public SeekBarPreference foregroundRed;

    /**
     * Persistent reference to "foreground_green" preference.
     */
    public SeekBarPreference foregroundGreen;

    /**
     * Persistent reference to "foreground_blue" preference.
     */
    public SeekBarPreference foregroundBlue;

    // endregion

    // region // Main.

    /**
     * Inflate preferences and get reference to every preference.
     *
     * @param key Root key from {@link #onCreatePreferences(Bundle, String)}.
     */
    protected void inflatePreferences(String key) {
        setPreferencesFromResource(R.xml.preferences, key);
        // Get references
        drawingType = findPreference("drawing_type");
        points = findPreference("points");
        paramN = findPreference("param_n");
        paramM = findPreference("param_m");
        lineWidth = findPreference("line_width");
        backgroundRed = findPreference("background_red");
        backgroundGreen = findPreference("background_green");
        backgroundBlue = findPreference("background_blue");
        foregroundRed = findPreference("foreground_red");
        foregroundGreen = findPreference("foreground_green");
        foregroundBlue = findPreference("foreground_blue");
    }

    /**
     * Inflate and initialize.
     */
    @Override
    public void onCreatePreferences(Bundle bundle, String key) {
        // Helper
        PrefsChangeListener helper = new PrefsChangeListener(this);
        // Inflate preferences
        inflatePreferences(key);
        // Set DPAD key increments
        points.setSeekBarIncrement(1);
        paramN.setSeekBarIncrement(1);
        paramM.setSeekBarIncrement(1);
        lineWidth.setSeekBarIncrement(1);
        // Lock drawing type in some flavors
        helper.lockDrawingType();
        // Connect preferences to actions
        helper.tieChange(drawingType);
        helper.tieChange(points);
        helper.tieChange(paramN);
        helper.tieChange(paramM);
        helper.tieChange(lineWidth);
        helper.tieChange(backgroundRed);
        helper.tieChange(backgroundGreen);
        helper.tieChange(backgroundBlue);
        helper.tieChange(foregroundRed);
        helper.tieChange(foregroundGreen);
        helper.tieChange(foregroundBlue);
    }

    // endregion

}
