package eu.pintergabor.wstar.ui.helper;

import android.view.KeyEvent;
import android.view.MenuItem;

import androidx.annotation.*;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;

import eu.pintergabor.wstar.R;
import eu.pintergabor.wstar.ui.MainActivity;

/**
 * Help {@link MainActivity} with handling buttons.
 */
public final class Buttons extends AutoHideToolbar implements Toolbar.OnMenuItemClickListener {

    // region // Constructor and link to MainActivity.

    /**
     * Configure buttons.
     */
    public Buttons(MainActivity mainActivity) {
        super(mainActivity);
        // Set up main toolbar
        inflateMenu(R.id.toolbar, R.menu.toolbar);
        // Set up small toolbars in Settings and About
        inflateMenu(R.id.settings_toolbar, R.menu.toolbar_back);
        inflateMenu(R.id.about_toolbar, R.menu.toolbar_back);
        // Show buttons in the beginning
        delayHide();
    }

    // endregion

    // region // Utils.

    /**
     * Inflate menu, set onClickListener, and enable icons in overflow menu.
     *
     * @param toolbarid Toolbar id.
     * @param menures   Menu id of the toolbar.
     */
    private void inflateMenu(@IdRes int toolbarid, @MenuRes int menures) {
        Toolbar toolbar = mainActivity.findViewById(toolbarid);
        MenuBuilder menu = (MenuBuilder) toolbar.getMenu();
        // Inflate toolbar menu.
        menu.clear();
        toolbar.inflateMenu(menures);
        // Set onClickListener.
        toolbar.setOnMenuItemClickListener(this);
        // Show icons in overflow menu.
        menu = (MenuBuilder) toolbar.getMenu();
        menu.setOptionalIconsVisible(true);
    }

    // endregion

    // region // Zoom and scroll actions.

    /**
     * Zoom steps as a result of button or keyboard key actions.
     */
    private final float ZOOMSTEP = 0.1f;

    /**
     * Zoom to default full screen view.
     */
    public void zoomNormal() {
        mainActivity.getMainHub().setScrollZoom(0.5f, 0.5f, 1.0f);
    }

    /**
     * Zoom in.
     */
    public void zoomIn() {
        mainActivity.getMainHub().onZoom(0.5f, 0.5f, 1f + ZOOMSTEP);
    }

    /**
     * Zoom out.
     */
    public void zoomOut() {
        mainActivity.getMainHub().onZoom(0.5f, 0.5f, 1f / (1f + ZOOMSTEP));
    }

    /**
     * Scroll steps as a result of button or keyboard key actions.
     */
    private final float SCROLLSTEP = 0.1f;

    /**
     * Scroll left.
     */
    public void scrollLeft() {
        mainActivity.getMainHub().onScroll(+SCROLLSTEP, 0f);
    }

    /**
     * Scroll right.
     */
    public void scrollRight() {
        mainActivity.getMainHub().onScroll(-SCROLLSTEP, 0f);
    }

    /**
     * Scroll up.
     */
    public void scrollUp() {
        mainActivity.getMainHub().onScroll(0f, +SCROLLSTEP);
    }

    /**
     * Scroll down.
     */
    public void scrollDown() {
        mainActivity.getMainHub().onScroll(0f, -SCROLLSTEP);
    }

    // endregion

    // region // View state change button and keyboard key actions

    /**
     * Change view state to SETTINGS or SETTINGS_MAIN.
     * <p>
     * Used to enter SETTINGS.
     */
    public void toSettings() {
        final ManageViews manageViews = mainActivity.getManageViews();
        final ManageViews.VIEWSTATE cvs = manageViews.getCurrentViewstate();
        final boolean portrait = manageViews.portrait();
        ManageViews.VIEWSTATE vs = ManageViews.VIEWSTATE.SETTINGS;
        switch (cvs) {
            case SETTINGS_MAIN:
            case ABOUT_MAIN:
            case MAIN:
                if (!portrait) {
                    vs = ManageViews.VIEWSTATE.SETTINGS_MAIN;
                }
                break;
        }
        manageViews.setCurrentViewstate(vs);
    }

    /**
     * Change view state to SETTINGS or SETTINGS_MAIN.
     * <p>
     * Used to enter ABOUT.
     */
    public void toAbout() {
        final ManageViews manageViews = mainActivity.getManageViews();
        final ManageViews.VIEWSTATE cvs = manageViews.getCurrentViewstate();
        final boolean portrait = manageViews.portrait();
        ManageViews.VIEWSTATE vs = ManageViews.VIEWSTATE.ABOUT;
        switch (cvs) {
            case SETTINGS_MAIN:
            case ABOUT_MAIN:
            case MAIN:
                if (!portrait) {
                    vs = ManageViews.VIEWSTATE.ABOUT_MAIN;
                }
                break;
        }
        manageViews.setCurrentViewstate(vs);
    }

    /**
     * Change view state to MAIN or to SETTINGS_MAIN or to ABOUT_MAIN.
     * <p>
     * Used to exit SETTINGS and ABOUT.
     */
    public void toMain() {
        final ManageViews manageViews = mainActivity.getManageViews();
        final ManageViews.VIEWSTATE cvs = manageViews.getCurrentViewstate();
        final boolean portrait = manageViews.portrait();
        ManageViews.VIEWSTATE vs = ManageViews.VIEWSTATE.MAIN;
        switch (cvs) {
            case SETTINGS:
            case SETTINGS_MAIN:
                if (!portrait) {
                    manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.SETTINGS_MAIN);
                }
                break;
            case ABOUT:
            case ABOUT_MAIN:
                if (!portrait) {
                    manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.ABOUT_MAIN);
                }
                break;
        }
        manageViews.setCurrentViewstate(vs);
    }

    /**
     * Call from {@link MainActivity} when the back button is pressed.
     */
    public boolean onBackPressed() {
        final ManageViews manageViews = mainActivity.getManageViews();
        final ManageViews.VIEWSTATE cvs = manageViews.getCurrentViewstate();
        final boolean portrait = manageViews.portrait();
        switch (cvs) {
            case ABOUT:
                if (portrait) {
                    manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.MAIN);
                } else {
                    manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.ABOUT_MAIN);
                }
                return true;
            case SETTINGS:
                if (portrait) {
                    manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.MAIN);
                } else {
                    manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.SETTINGS_MAIN);
                }
                return true;
            case ABOUT_MAIN:
            case SETTINGS_MAIN:
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.MAIN);
                return true;
        }
        return false;
    }

    /**
     * Help {@link #onKeyDown(int, KeyEvent)}.
     */
    private void showMain() {
        final ManageViews manageViews = mainActivity.getManageViews();
        final ManageViews.VIEWSTATE cvs = manageViews.getCurrentViewstate();
        if (cvs != ManageViews.VIEWSTATE.MAIN &&
            cvs != ManageViews.VIEWSTATE.SETTINGS_MAIN &&
            cvs != ManageViews.VIEWSTATE.ABOUT_MAIN) {
            manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.MAIN);
        }
    }

    /**
     * Call from {@link MainActivity} when a key is is pressed.
     */
    public boolean onKeyDown(int keyCode, @SuppressWarnings("unused") KeyEvent event) {
        final ManageViews manageViews = mainActivity.getManageViews();
        switch (keyCode) {
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_F1:
            case KeyEvent.KEYCODE_A:
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.ABOUT);
                return true;
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_F2:
            case KeyEvent.KEYCODE_S:
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.SETTINGS);
                return true;
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_F3:
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.ABOUT_MAIN);
                return true;
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_F4:
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.SETTINGS_MAIN);
                return true;
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_F5:
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_NUMPAD_ENTER:
            case KeyEvent.KEYCODE_W:
            case KeyEvent.KEYCODE_M:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.MAIN);
                return true;
            case KeyEvent.KEYCODE_PAGE_DOWN:
            case KeyEvent.KEYCODE_NUMPAD_SUBTRACT:
                showMain();
                zoomOut();
                return true;
            case KeyEvent.KEYCODE_PAGE_UP:
            case KeyEvent.KEYCODE_NUMPAD_ADD:
                showMain();
                zoomIn();
                return true;
            case KeyEvent.KEYCODE_SPACE:
            case KeyEvent.KEYCODE_NUMPAD_MULTIPLY:
                showMain();
                zoomNormal();
                return true;
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_NUMPAD_4:
                showMain();
                scrollLeft();
                return true;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
            case KeyEvent.KEYCODE_NUMPAD_6:
                showMain();
                scrollRight();
                return true;
            case KeyEvent.KEYCODE_DPAD_UP:
            case KeyEvent.KEYCODE_NUMPAD_8:
                showMain();
                scrollUp();
                return true;
            case KeyEvent.KEYCODE_DPAD_DOWN:
            case KeyEvent.KEYCODE_NUMPAD_2:
                showMain();
                scrollDown();
                return true;
        }
        return false;
    }

    @Override
    public boolean onMenuItemClick(@NonNull MenuItem item) {
        delayHide();
        switch (item.getItemId()) {
            case R.id.action_settings:
                toSettings();
                break;
            case R.id.action_zoom_full:
                zoomNormal();
                break;
            case R.id.action_zoom_in:
                zoomIn();
                break;
            case R.id.action_zoom_out:
                zoomOut();
                break;
            case R.id.action_save_HD:
                mainActivity.getSave().save(1920, 1080);
                break;
            case R.id.action_save_4K:
                mainActivity.getSave().save(3840, 2160);
                break;
            case R.id.action_save_8K:
                mainActivity.getSave().save(7680, 4320);
                break;
            case R.id.action_save_custom:
                final SizeDialogFragment dialog = new SizeDialogFragment();
                dialog.show(mainActivity.getSupportFragmentManager(), null);
                break;
            case R.id.action_about:
                toAbout();
                break;
            case R.id.action_back:
                toMain();
                break;
            case R.id.action_close:
                mainActivity.finish();
                break;
        }
        return true;
    }

    // endregion

}
