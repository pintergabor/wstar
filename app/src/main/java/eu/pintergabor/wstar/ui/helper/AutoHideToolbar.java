package eu.pintergabor.wstar.ui.helper;

import android.os.Handler;
import android.view.View;

import androidx.appcompat.widget.Toolbar;

import eu.pintergabor.wstar.R;
import eu.pintergabor.wstar.ui.MainActivity;

/**
 * Auto hide toolbar.
 */
public class AutoHideToolbar {

    // region // Constructor and link to MainActivity.

    /**
     * Link to {@link MainActivity}.
     */
    protected final MainActivity mainActivity;

    public AutoHideToolbar(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    // endregion

    // region // Auto hide toolbar.

    /**
     * Auto hide time in milliseconds.
     */
    private final int HIDETIME = 5000;

    /**
     * Handle {@link #runHide}.
     */
    private final Handler handler = new Handler();

    /**
     * Hide toolbar.
     */
    public void hide() {
        Toolbar toolbar = mainActivity.findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setVisibility(View.GONE);
        }
    }

    /**
     * Show toolbar.
     */
    public void show() {
        handler.removeCallbacks(runHide);
        Toolbar toolbar = mainActivity.findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Encapsulate {@link #hide()}.
     */
    private Runnable runHide = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    /**
     * Show toolbar and auto hide it after {@link #HIDETIME}.
     */
    public void delayHide() {
        show();
        handler.postDelayed(runHide, HIDETIME);
    }

}
