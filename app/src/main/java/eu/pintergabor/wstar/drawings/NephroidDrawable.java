package eu.pintergabor.wstar.drawings;

import android.graphics.Canvas;
import android.graphics.Paint;

import androidx.annotation.NonNull;

/**
 * Drawable Nephroid image.
 */
public class NephroidDrawable extends WDrawable {

    // region // Create and init.

    /**
     * Just create an empty object.
     * <p>
     * Call {@link #init(int, float, float, float)} later to initialize.
     */
    @SuppressWarnings("unused")
    public NephroidDrawable() {
        super();
    }

    // endregion

    // region // Draw.

    /**
     * Draw the image.
     *
     * @param canvas Canvas to draw on.
     */
    @Override
    protected void drawForeground(@NonNull Canvas canvas) {
        pen.setColor(getForeColor());
        pen.setStyle(Paint.Style.STROKE);
        pen.setStrokeWidth(getLineWidth());
        final int s = points.getSize();
        final float centerx = getCenterX();
        for (int i = 0; i < s; i++) {
            canvas.drawCircle(
                points.getX(i), points.getY(i),
                Math.abs(points.getX(i) - centerx),
                pen);
        }
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        // Get the drawable's bounds
        final float width = getBounds().width();
        final float height = getBounds().height();
        final float centerx = width * getPanX();
        final float centery = height * getPanY();
        final float radius = Math.min(width / 2f, height / 1.4142f) * getZoom() / 2f;
        // Calculate points
        init(getSize(), centerx, centery, radius);
        // Draw background
        drawBackground(canvas);
        // Draw WSTAR
        drawForeground(canvas);
    }

    // endregion

}
