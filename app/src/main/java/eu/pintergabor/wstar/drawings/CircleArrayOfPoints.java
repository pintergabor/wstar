package eu.pintergabor.wstar.drawings;

/**
 * Efficient calculation and storage of an array of points
 * equidistant on the perimeter of a circle.
 */
@SuppressWarnings("unused")
public class CircleArrayOfPoints extends ArrayOfPoints {

    /**
     * Center X coordinate.
     */
    private float centerX;

    /**
     * Center Y coordinate.
     */
    private float centerY;

    /**
     * Radius.
     */
    private float radius = Float.NaN;

    @Override
    public void setSize(int size) {
        if (getSize() != size) {
            // Create array
            super.setSize(size);
            // Invalidate array contents
            centerX = 0;
            centerY = 0;
            radius = Float.NaN;
        }
    }

    /**
     * Create an empty array of points.
     */
    @SuppressWarnings("unused")
    public CircleArrayOfPoints() {
        // Nothing
    }

    /**
     * Create an empty array of points.
     *
     * @param size Size of the array.
     */
    @SuppressWarnings("unused")
    public CircleArrayOfPoints(int size) {
        // Create array
        super(size);
    }

    /**
     * Create an array of points and fill it with points
     * equidistant on the perimeter of a circle.
     *
     * @param size Number of points.
     * @param x    Center X coordinate.
     * @param y    Center Y coordinate.
     * @param r    Radius.
     */
    @SuppressWarnings("unused")
    public CircleArrayOfPoints(int size, float x, float y, float r) {
        // Create array
        super(size);
        // Fill it with data
        calculate(x, y, r);
    }

    /**
     * Calculate coordinates of points
     * equidistant on the perimeter of a circle.
     *
     * @param x Center X coordinate.
     * @param y Center Y coordinate.
     * @param r Radius.
     */
    @SuppressWarnings("unused")
    public void calculate(float x, float y, float r) {
        if (centerX != x || centerY != y || radius != r) {
            centerX = x;
            centerY = y;
            radius = r;
            double slice = 2 * Math.PI / getSize();
            for (int i = 0; i < getSize(); i++) {
                double islice = i * slice;
                setPoint(i,
                    Math.round(x + r * Math.sin(islice)),
                    Math.round(y + r * Math.cos(islice)));
            }
        }
    }

}
