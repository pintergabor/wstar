package eu.pintergabor.wstar.drawings;

import android.graphics.Canvas;
import android.graphics.Paint;

import androidx.annotation.NonNull;

/**
 * Drawable WSTAR image.
 */
public class PascalDrawable extends WDrawable {

    // region // Create and init.

    /**
     * Just create an empty object.
     * <p>
     * Call {@link #init(int, float, float, float)} later to initialize.
     */
    @SuppressWarnings("unused")
    public PascalDrawable() {
        super();
    }

    // endregion

    // region // Draw.

    /**
     * Draw the image.
     *
     * @param canvas Canvas to draw on.
     */
    @Override
    protected void drawForeground(@NonNull Canvas canvas) {
        pen.setColor(getForeColor());
        pen.setStyle(Paint.Style.STROKE);
        pen.setStrokeWidth(getLineWidth());
        final int s = getSize();
        final int n = getParamN();
        final int m = getParamM();
        int i = 0, j = 0;
        do {
            j = (j + m) % s;
            i = (i + n) % s;
            canvas.drawLine(
                points.getX(i), points.getY(i),
                points.getX(j), points.getY(j),
                pen);
        } while (i != 0 || j != 0);
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        // Get the drawable's bounds
        final float width = getBounds().width();
        final float height = getBounds().height();
        final float centerx = width * getPanX();
        final float centery = height * getPanY();
        final float radius = Math.min(width, height) * getZoom() / 2f;
        // Calculate points
        init(getSize(), centerx, centery, radius);
        // Draw background
        drawBackground(canvas);
        // Draw WSTAR
        drawForeground(canvas);
    }

    // endregion

}
