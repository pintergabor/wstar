package eu.pintergabor.wstar.drawings;

import android.graphics.Canvas;
import android.graphics.Paint;

import androidx.annotation.*;

/**
 * Base class defining {@link #drawBackground(Canvas)} and {@link #drawForeground(Canvas)}
 * for any drawable that uses points equidistant on the perimeter of a circle.
 */
public abstract class WDrawable extends WDrawableBase {

    /**
     * To reduce memory allocation in {@link #draw(Canvas)}.
     */
    protected Paint pen = new Paint();

    /**
     * Draw the background.
     *
     * @param canvas Canvas to draw on.
     */
    protected void drawBackground(@NonNull Canvas canvas) {
        canvas.drawColor(getBackColor());
    }

    /**
     * Draw the image.
     *
     * @param canvas Canvas to draw on.
     */
    protected abstract void drawForeground(@NonNull Canvas canvas);

}
