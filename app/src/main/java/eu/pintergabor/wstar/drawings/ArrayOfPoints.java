package eu.pintergabor.wstar.drawings;

import android.graphics.PointF;

/**
 * Efficient storage for an array of points.
 */
public class ArrayOfPoints {

    /**
     * Number of points.
     */
    private int sizePoints;

    /**
     * The coordinates of the points.
     * <p>
     * points[i][0] is the X coordinate of point i.<br/>
     * points[i][1] is the Y coordinate of point i.
     */
    private float[][] points;

    /**
     * Get the size of the array.
     *
     * @return The size of the array.
     */
    @SuppressWarnings("unused")
    public int getSize() {
        return sizePoints;
    }

    /**
     * Set, or change, the size of the array.
     *
     * @param size The new size of the array.
     */
    @SuppressWarnings("unused")
    public void setSize(int size){
        if (size != sizePoints) {
            points = new float[size][2];
            sizePoints = size;
        }
    }

    /**
     * Create an array of points.
     */
    @SuppressWarnings("unused")
    public ArrayOfPoints() {
        // Nothing
    }

    /**
     * Create an array of points.
     *
     * @param size The size of the array.
     */
    @SuppressWarnings("unused")
    public ArrayOfPoints(int size) {
        setSize(size);
    }

    /**
     * Get X coordinate of point i.
     *
     * @param i Index.
     * @return The X coordinate of point i.
     */
    @SuppressWarnings("unused")
    public float getX(int i) {
        if (0 <= i && i < sizePoints) {
            return points[i][0];
        }
        return 0;
    }

    /**
     * Set X coordinate of point i.
     *
     * @param i Index.
     * @param x X coordinate.
     */
    @SuppressWarnings("unused")
    public void setX(int i, float x) {
        if (0 <= i && i < sizePoints) {
            points[i][0] = x;
        }
    }

    /**
     * Get Y coordinate of point i.
     *
     * @param i Index.
     * @return The Y coordinate of point i.
     */
    @SuppressWarnings("unused")
    public float getY(int i) {
        if (0 <= i && i < sizePoints) {
            return points[i][1];
        }
        return 0;
    }

    /**
     * Set Y coordinates of point i.
     *
     * @param i Index.
     * @param y Y coordinate.
     */
    @SuppressWarnings("unused")
    public void setY(int i, float y) {
        if (0 <= i && i < sizePoints) {
            points[i][1] = y;
        }
    }

    /**
     * Get {@link android.graphics.Point} i.
     *
     * @param i Index.
     * @return The point.
     */
    @SuppressWarnings("unused")
    public PointF getPoint(int i) {
        if (0 <= i && i < sizePoints) {
            return new PointF(points[i][0], points[i][1]);
        }
        return new PointF(0, 0);
    }

    /**
     * Set {@link android.graphics.Point} i.
     *
     * @param i Index.
     * @param p Point.
     */
    @SuppressWarnings("unused")
    public void setPoint(int i, PointF p) {
        if (0 <= i && i < sizePoints) {
            points[i][0] = p.x;
            points[i][1] = p.y;
        }
    }

    /**
     * Set coordinates of point i.
     *
     * @param i Index.
     * @param x X coordinate.
     * @param y Y coordinate.
     */
    @SuppressWarnings("unused")
    public void setPoint(int i, float x, float y) {
        if (0 <= i && i < sizePoints) {
            points[i][0] = x;
            points[i][1] = y;
        }
    }

}
