package eu.pintergabor.wstar.drawings;

import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;

import androidx.annotation.Nullable;

/**
 * Base class for any drawable that uses points equidistant on the perimeter of a circle and has the following properties:
 * <p>
 * {@link #getSize()}/{@link #setSize(int)} Number of points.<br/>
 * {@link #getParamN()} /{@link #setParamN(int)} Parameter N.<br/>
 * {@link #getParamM()} /{@link #setParamM(int)} Parameter M.<br/>
 * {@link #getLineWidth()} /{@link #setLineWidth(float)} Line width.<br/>
 * {@link #getZoom()} /{@link #setZoom(float)} Zoom level. (=1.0 for full screen view)<br/>
 * {@link #getPanX()} /{@link #setScrollX(float)} Pan X coordinate (=0.5 for center)<br/>
 * {@link #getPanY()} /{@link #setScrollY(float)} Pan Y coordinate (=0.5 for center)<br/>
 * {@link #getForeColor()} /{@link #setForeColor(int)} Foreground color.<br/>
 * {@link #getBackColor()} /{@link #setBackColor(int)} Background color.<br/>
 */
public abstract class WDrawableBase extends Drawable {

    // region // Number of points and parameter N and M.

    /**
     * Coordinates of points equidistant on the perimeter of a circle.
     */
    protected CircleArrayOfPoints points;

    /**
     * Get the number of points.
     *
     * @return The number of points
     */
    @SuppressWarnings("unused")
    public int getSize() {
        if (points != null) {
            return points.getSize();
        }
        return 0;
    }

    /**
     * Define the number of points.
     */
    @SuppressWarnings("unused")
    public void setSize(int size) {
        if (getSize() != size || points == null) {
            points = new CircleArrayOfPoints(size);
            invalidateSelf();
        }
    }

    private int paramN = 1;

    /**
     * Parameter N.
     *
     * @return Parameter N
     */
    @SuppressWarnings("unused")
    public int getParamN() {
        return paramN;
    }

    /**
     * Define parameter N.
     *
     * @param paramN New value
     */
    @SuppressWarnings("unused")
    public void setParamN(int paramN) {
        this.paramN = paramN;
        invalidateSelf();
    }

    private int paramM = 1;

    /**
     * Parameter M.
     *
     * @return Parameter M
     */
    @SuppressWarnings("unused")
    public int getParamM() {
        return paramM;
    }

    /**
     * Define parameter M.
     *
     * @param paramM New value
     */
    @SuppressWarnings("unused")
    public void setParamM(int paramM) {
        this.paramM = paramM;
        invalidateSelf();
    }

    // endregion

    // region // Line width.

    /**
     * Line width.
     */
    private float lineWidth = 1.0f;

    /**
     * Get line width.
     *
     * @return Line width.
     */
    @SuppressWarnings("unused")
    public float getLineWidth() {
        return lineWidth;
    }

    /**
     * Set line width.
     *
     * @param value new width in pixels
     */
    @SuppressWarnings("unused")
    public void setLineWidth(float value) {
        lineWidth = value;
        invalidateSelf();
    }

    // endregion

    // region // Zoom.

    /**
     * Zoom.
     */
    private float zoom = 1.0f;

    /**
     * Get zoom.
     *
     * @return Zoom.
     */
    @SuppressWarnings("unused")
    public float getZoom() {
        return zoom;
    }

    /**
     * Set zoom.
     *
     * @param value Zoom
     */
    @SuppressWarnings("unused")
    public void setZoom(float value) {
        zoom = value;
        invalidateSelf();
    }

    // endregion

    // region // Pan

    /**
     * Pan X.
     */
    private float panX = 0.5f;

    /**
     * Pan X.
     *
     * @return Pan X.
     */
    @SuppressWarnings("unused")
    public float getPanX() {
        return panX;
    }

    /**
     * Set pan X.
     *
     * @param value Pan X
     */
    @SuppressWarnings("unused")
    public void setScrollX(float value) {
        panX = value;
        invalidateSelf();
    }

    /**
     * Pan Y.
     */
    private float panY = 0.5f;

    /**
     * Pan Y.
     *
     * @return Pan Y.
     */
    @SuppressWarnings("unused")
    public float getPanY() {
        return panY;
    }

    /**
     * Set pan Y.
     *
     * @param value Pan Y
     */
    @SuppressWarnings("unused")
    public void setScrollY(float value) {
        panY = value;
        invalidateSelf();
    }

    // endregion

    // region // Background color.

    /**
     * Background color.
     */
    private int backColor = Color.WHITE;

    /**
     * Get background color.
     *
     * @return Background color.
     */
    @SuppressWarnings("unused")
    public int getBackColor() {
        return backColor;
    }

    /**
     * Set background color.
     *
     * @param value new background color.
     */
    @SuppressWarnings("unused")
    public void setBackColor(int value) {
        backColor = value;
        invalidateSelf();
    }

    // endregion

    // region // Foreground color.

    /**
     * Foreground color.
     */
    private int foreColor = Color.BLACK;

    /**
     * Get foreground color.
     *
     * @return Foreground color.
     */
    @SuppressWarnings("unused")
    public int getForeColor() {
        return foreColor;
    }

    /**
     * Set foreground color.
     *
     * @param value new foreground color.
     */
    @SuppressWarnings("unused")
    public void setForeColor(int value) {
        foreColor = value;
        invalidateSelf();
    }

    // endregion

    // region // Create and init.

    private float centerX = Float.NaN;

    /**
     * Get X coordinate of the center.
     *
     * @return X coordinate of the center.
     */
    @SuppressWarnings("unused")
    public float getCenterX() {
        return centerX;
    }

    private float centerY = Float.NaN;

    /**
     * Get Y coordinate of the center.
     *
     * @return Y coordinate of the center.
     */
    @SuppressWarnings("unused")
    public float getCenterY() {
        return centerY;
    }

    private float radius = Float.NaN;

    /**
     * Get radius of the circle.
     *
     * @return Radios of the circle.
     */
    @SuppressWarnings("unused")
    public float getRadius() {
        return radius;
    }

    /**
     * Just create an empty object.
     * <p>
     * Call {@link #init(int, float, float, float)} later to initialize.
     */
    @SuppressWarnings("unused")
    public WDrawableBase() {
        super();
    }

    /**
     * Calculate coordinates.
     *
     * @param x X coordinate of the center.
     * @param y Y coordinate of the center.
     * @param r Radius.
     */
    private void init(float x, float y, float r) {
        // Store parameters
        centerX = x;
        centerY = y;
        radius = r;
        // Calculate coordinates
        points.calculate(x, y, r);
    }

    /**
     * Set size and calculate coordinates.
     *
     * @param size Number of points.
     * @param x    X coordinate of the center.
     * @param y    Y coordinate of the center.
     * @param r    Radius.
     */
    protected void init(int size, float x, float y, float r) {
        // Create the array if it does not exist, or if it is the wrong size
        setSize(size);
        // Calculate coordinates
        init(x, y, r);
    }

    // endregion

    // region // Required methods.

    @Override
    public void setAlpha(int alpha) {
        // This method is required
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        // This method is required
    }

    @Override
    public int getOpacity() {
        // Must be PixelFormat.UNKNOWN, TRANSLUCENT, TRANSPARENT, or OPAQUE
        return PixelFormat.OPAQUE;
    }

    // endregion

}
