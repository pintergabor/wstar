package eu.pintergabor.wstar.drawings;

import android.graphics.Canvas;
import android.graphics.Paint;

import androidx.annotation.*;

/**
 * Drawable WSTAR image.
 */
public class WstarDrawable extends WDrawable {

    // region // Draw.

    /**
     * Draw WSTAR on Canvas.
     *
     * @param canvas Canvas to draw on.
     */
    @Override
    protected void drawForeground(@NonNull Canvas canvas) {
        pen.setColor(getForeColor());
        pen.setStyle(Paint.Style.STROKE);
        pen.setStrokeWidth(getLineWidth());
        int size = getSize();
        int n = getParamN();
        int m = getParamM();
        // Ensure n<=m
        if (m < n) {
            int x = m;
            m = n;
            n = x;
        }
        // n<=m<=Max diagonal length
        int maxm = size / 2;
        n = Math.min(n, maxm);
        m = Math.min(m, maxm);
        // For each diagonal which has length of [n..m] inclusive
        for (int i = n; i <= m; i++) {
            // There are 'size' diagonals, except if 'size' is even and 'length' is exactly 'size'/2 ...
            int ms = size;
            if (2 * i == size) {
                // ... then there are 'size'/2 diagonals.
                ms = size / 2;
            }
            // Draw diagonals
            for (int j = 0; j < ms; j++) {
                int ij = i + j;
                if (size <= ij) {
                    ij -= size;
                }
                canvas.drawLine(
                    points.getX(j), points.getY(j),
                    points.getX(ij), points.getY(ij),
                    pen);
            }
        }
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        // Get the drawable's bounds
        final float width = getBounds().width();
        final float height = getBounds().height();
        final float centerx = width * getPanX();
        final float centery = height * getPanY();
        final float radius = Math.min(width, height) * getZoom() / 2f;
        // Calculate points
        init(getSize(), centerx, centery, radius);
        // Draw background
        drawBackground(canvas);
        // Draw WSTAR
        drawForeground(canvas);
    }

    // endregion

}
