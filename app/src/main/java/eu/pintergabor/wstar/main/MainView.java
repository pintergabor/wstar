package eu.pintergabor.wstar.main;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.*;

import eu.pintergabor.util.view.AdditionalViewUtils;
import eu.pintergabor.wstar.ui.MainActivity;

/**
 * Full screen view containing the drawing.
 */
public class MainView extends SurfaceView {

    // region // Constructors

    /**
     * As required + local init.
     */
    public MainView(Context context) {
        super(context);
        init(null);
    }

    /**
     * As required + local init.
     */
    public MainView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    /**
     * As required + local init.
     */
    public MainView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    /**
     * As required + local init.
     */
    @RequiresApi(21)
    @SuppressWarnings("unused")
    public MainView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    // endregion

    // region // Drawing control.

    private DrawingThread drawingThread;

    /**
     * Called at the end of every constructor to initialize custom fields.
     *
     * @param attrs Attribute set.
     */
    private void init(@Nullable @SuppressWarnings("unused") AttributeSet attrs) {
        // Nothing.
    }

    /**
     * Get {@link MainHub}.
     *
     * @return The only {@link MainHub} instance from {@link MainActivity}.
     */
    private MainHub getMainHub() {
        Activity activity = AdditionalViewUtils.getActivity(this);
        if (activity instanceof MainActivity) {
            return ((MainActivity) activity).getMainHub();
        }
        return null;
    }

    /**
     * Create and activate drawing thread.
     */
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        final MainHub mainHub = getMainHub();
        if (mainHub != null) {
            // Create drawing thread.
            drawingThread = new DrawingThread(mainHub);
            drawingThread.start();
            // Draw on the surface when needed.
            final SurfaceHolder surfaceHolder = getHolder();
            surfaceHolder.addCallback(drawingThread);
            // Draw is needed now, and then every time when parameters change.
            mainHub.setRedrawRequestListener(drawingThread);
            drawingThread.redrawRequest();
        }
    }

    /**
     * Destroy drawing thread.
     */
    @Override
    protected void onDetachedFromWindow() {
        // Destroy drawing thread.
        if (drawingThread != null) {
            drawingThread.terminateRun();
            drawingThread = null;
        }
        //
        super.onDetachedFromWindow();
    }

    // endregion

}
