package eu.pintergabor.wstar.main;

import android.graphics.Color;

import eu.pintergabor.wstar.ui.MainFragment;
import eu.pintergabor.wstar.ui.PrefsFragment;

/**
 * Communication interface between {@link PrefsFragment}, {@link MainFragment} and {@link MainView}.
 */
public final class MainHub {

    // region // Redraw requests.

    private RedrawRequestListener redrawRequestListener;

    /**
     * Set callback on redraw requests.
     *
     * @param listener Callback.
     */
    public void setRedrawRequestListener(RedrawRequestListener listener) {
        redrawRequestListener = listener;
    }

    /**
     * Callback on redraw request.
     */
    public interface RedrawRequestListener {
        /**
         * Called on request redraw.
         */
        void redrawRequest();
    }

    /**
     * Request redraw.
     */
    public void redrawRequest() {
        if (redrawRequestListener != null) {
            redrawRequestListener.redrawRequest();
        }
    }

    // endregion

    // region // Parameters.

    // region // Drawing type.

    /**
     * {@link #getDrawingType()}
     */
    private String drawingType;

    /**
     * Get drawing type.
     * <p>
     * Drawing types are hard codes strings, like "wstar", "pascal" or "nephroid".
     */
    public synchronized String getDrawingType() {
        return drawingType;
    }

    /**
     * {@link #getDrawingType()}
     */
    public synchronized void setDrawingType(String value) {
        drawingType = value;
    }

    // endregion

    // region // Points.

    /**
     * {@link #getPoints()}
     */
    private int points;

    /**
     * Get the number of points.
     */
    public synchronized int getPoints() {
        return points;
    }

    /**
     * {@link #getPoints()}
     */
    public synchronized void setPoints(int value) {
        points = value;
    }

    // endregion

    // region // Parameter N.

    /**
     * {@link #getParamN()}}
     */
    private int paramN;

    /**
     * Get parameter N.
     */
    public synchronized int getParamN() {
        return paramN;
    }

    /**
     * {@link #getParamN()}}
     */
    public synchronized void setParamN(int value) {
        paramN = value;
    }

    // endregion

    // region // Parameter M.

    /**
     * {@link #getParamM()}}
     */
    private int paramM;

    /**
     * Get parameter M.
     */
    public synchronized int getParamM() {
        return paramM;
    }

    /**
     * {@link #getParamM()}}
     */
    public synchronized void setParamM(int value) {
        paramM = value;
    }

    // endregion

    // region // Line width.

    /**
     * {@link #getLineWidth()}
     */
    private float lineWidth;

    /**
     * Get line width.
     */
    public synchronized float getLineWidth() {
        return lineWidth;
    }

    /**
     * {@link #getLineWidth()}
     */
    public synchronized void setLineWidth(float value) {
        lineWidth = value;
    }

    // endregion

    // region // Background color.

    /**
     * {@link #getBackColor()}
     */
    private int backColor = Color.WHITE;

    /**
     * Get background color.
     */
    @SuppressWarnings("unused")
    public synchronized int getBackColor() {
        return backColor;
    }

    /**
     * Get background color red component.
     */
    @SuppressWarnings("unused")
    public synchronized int getBackRed() {
        return Color.red(backColor);
    }

    /**
     * Get background color green component.
     */
    @SuppressWarnings("unused")
    public synchronized int getBackGreen() {
        return Color.green(backColor);
    }

    /**
     * Get background color blue component.
     */
    @SuppressWarnings("unused")
    public synchronized int getBackBlue() {
        return Color.blue(backColor);
    }

    /**
     * {@link #getBackColor()}
     */
    @SuppressWarnings("unused")
    public synchronized void setBackColor(int value) {
        backColor = value;
    }

    /**
     * Set background color red component.
     */
    @SuppressWarnings("unused")
    public synchronized void setBackRed(int value) {
        backColor = Color.rgb(
            value,
            Color.green(backColor),
            Color.blue(backColor));
    }

    /**
     * Set background color green component.
     */
    @SuppressWarnings("unused")
    public synchronized void setBackGreen(int value) {
        backColor = Color.rgb(
            Color.red(backColor),
            value,
            Color.blue(backColor));
    }

    /**
     * Set background color blue component.
     */
    @SuppressWarnings("unused")
    public synchronized void setBackBlue(int value) {
        backColor = Color.rgb(
            Color.red(backColor),
            Color.green(backColor),
            value);
    }

    // endregion

    // region // Foreground color.

    /**
     * {@link #getForeColor()}
     */
    private int foreColor = Color.BLACK;

    /**
     * Get foreground color.
     */
    @SuppressWarnings("unused")
    public synchronized int getForeColor() {
        return foreColor;
    }

    /**
     * Get foreground color red component.
     */
    @SuppressWarnings("unused")
    public synchronized int getForeRed() {
        return Color.red(foreColor);
    }

    /**
     * Get foreground color green component.
     */
    @SuppressWarnings("unused")
    public synchronized int getForeGreen() {
        return Color.green(foreColor);
    }

    /**
     * Get foreground color blue component.
     */
    @SuppressWarnings("unused")
    public synchronized int getForeBlue() {
        return Color.blue(foreColor);
    }

    /**
     * {@link #getForeColor()}
     */
    @SuppressWarnings("unused")
    public synchronized void setForeColor(int value) {
        foreColor = value;
    }

    /**
     * Set foreground color red component.
     */
    @SuppressWarnings("unused")
    public synchronized void setForeRed(int value) {
        foreColor = Color.rgb(
            value,
            Color.green(foreColor),
            Color.blue(foreColor));
    }

    /**
     * Set foreground color green component.
     */
    @SuppressWarnings("unused")
    public synchronized void setForeGreen(int value) {
        foreColor = Color.rgb(
            Color.red(foreColor),
            value,
            Color.blue(foreColor));
    }

    /**
     * Set foreground color blue component.
     */
    @SuppressWarnings("unused")
    public synchronized void setForeBlue(int value) {
        foreColor = Color.rgb(
            Color.red(foreColor),
            Color.green(foreColor),
            value);
    }

    // endregion

    // endregion

    // region // Scroll and zoom.

    /**
     * {@link #getScrollX()}
     */
    private float scrollX = 0.5f;

    /**
     * Get scroll X.
     * <p>
     * 0.5 means the center of the visible area.
     */
    public float getScrollX() {
        return scrollX;
    }

    /**
     * {@link #getScrollX()}
     */
    public void setScrollX(float scrollX) {
        this.scrollX = scrollX;
    }

    /**
     * {@link #getScrollY()}
     */
    private float scrollY = 0.5f;

    /**
     * Get scroll Y, relative to screen size.
     * <p>
     * 0.5 means the center of the visible area.
     */
    public float getScrollY() {
        return scrollY;
    }

    public void setScrollY(float scrollY) {
        this.scrollY = scrollY;
    }

    /**
     * {@link #getZoom()}
     */
    private float zoom = 1f;

    /**
     * Get zoom level.
     * <p>
     * 1 means no zoom.
     */
    public float getZoom() {
        return zoom;
    }

    /**
     * {@link #getZoom()}
     */
    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    /**
     * Change scroll by scrollX, scrollY.
     */
    public void onScroll(float scrollX, float scrollY) {
        final float x = getScrollX();
        final float y = getScrollY();
        setScrollX(x - scrollX);
        setScrollY(y - scrollY);
        // Request redraw
        redrawRequest();
    }

    /**
     * Change zoom.
     *
     * @param centerX Center X coordinate of the zoom change.
     * @param centerY Center Y coordinate of the zoom change.
     * @param zoom    The zoom change.
     */
    public void onZoom(float centerX, float centerY, float zoom) {
        // Center offset compensation
        final float x = getScrollX();
        final float y = getScrollY();
        setScrollX(centerX * (1f - zoom) + x * zoom);
        setScrollY(centerY * (1f - zoom) + y * zoom);
        // Zoom
        setZoom(getZoom() * zoom);
        // Request redraw
        redrawRequest();
    }

    /**
     * Set scroll and zoom to absolute values.
     * <p>
     * Usually used to reset to default, full screen (0.5, 0.5, 1.0) values
     *
     * @param scrollX New scroll X.
     * @param scrollY New scroll Y.
     * @param zoom    New zoom.
     */
    public void setScrollZoom(float scrollX, float scrollY, float zoom) {
        setScrollX(scrollX);
        setScrollY(scrollY);
        setZoom(zoom);
        // Request redraw
        redrawRequest();
    }

    // endregion

}
