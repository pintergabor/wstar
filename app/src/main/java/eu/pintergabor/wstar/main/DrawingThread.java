package eu.pintergabor.wstar.main;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

import androidx.annotation.*;

import eu.pintergabor.util.thread.ControlledThread;
import eu.pintergabor.wstar.drawings.NephroidDrawable;
import eu.pintergabor.wstar.drawings.PascalDrawable;
import eu.pintergabor.wstar.drawings.WDrawableBase;
import eu.pintergabor.wstar.drawings.WstarDrawable;

/**
 * The drawing thread.
 */
public final class DrawingThread extends ControlledThread
    implements SurfaceHolder.Callback, MainHub.RedrawRequestListener {

    // region // Constructor and link to MainHub.

    /**
     * Link to {@link MainHub}.
     */
    private final MainHub mainHub;

    public DrawingThread(@NonNull MainHub mainHub) {
        super();
        this.mainHub = mainHub;
    }

    // endregion

    // region // Draw.

    /**
     * The image.
     */
    private WDrawableBase wimage;

    /**
     * Update all parameters and request redraw.
     */
    public void redrawRequest() {
        // Recreate image if type is changed
        final String type = mainHub.getDrawingType();
        switch (type) {
            default: // === case "wstar":
                if (!(wimage instanceof WstarDrawable)) {
                    wimage = new WstarDrawable();
                }
                break;
            case "pascal":
                if (!(wimage instanceof PascalDrawable)) {
                    wimage = new PascalDrawable();
                }
                break;
            case "nephroid":
                if (!(wimage instanceof NephroidDrawable)) {
                    wimage = new NephroidDrawable();
                }
                break;
        }
        // Update parameters
        wimage.setSize(mainHub.getPoints());
        wimage.setParamN(mainHub.getParamN());
        wimage.setParamM(mainHub.getParamM());
        wimage.setLineWidth(mainHub.getLineWidth());
        wimage.setBackColor(mainHub.getBackColor());
        wimage.setForeColor(mainHub.getForeColor());
        wimage.setScrollX(mainHub.getScrollX());
        wimage.setScrollY(mainHub.getScrollY());
        wimage.setZoom(mainHub.getZoom());
        // Request redraw
        resumeRun();
    }

    // endregion

    // region // Thread control.

    /**
     * Create drawings here.
     * <p>
     * Valid from {@link #surfaceCreated(SurfaceHolder)} till {@link #surfaceDestroyed(SurfaceHolder)}.
     */
    private SurfaceHolder surfaceHolder;

    /**
     * Synchronize access to {@link #surfaceHolder}.
     */
    private final Object lockSurfaceHolder = new Object();

    /**
     * Start creating drawings.
     *
     * @param holder Here.
     */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceHolder = holder;
        resumeRun();
    }

    /**
     * Request redraw if something changes.
     */
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        resumeRun();
    }

    /**
     * Stop creating drawings.
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (surfaceHolder != null) {
            synchronized (lockSurfaceHolder) {
                surfaceHolder = null;
            }
        }
    }

    @Override
    public void run() {
        while (isRunning()) {
            // Draw a drawing
            synchronized (lockSurfaceHolder) {
                if (surfaceHolder != null) {
                    final Canvas canvas = surfaceHolder.lockCanvas();
                    if (wimage != null) {
                        wimage.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                        wimage.draw(canvas);
                    }
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
            // Pause until someone calls resumeRune.
            pauseRun();
            mayPause();
        }
    }

    // endregion

}
