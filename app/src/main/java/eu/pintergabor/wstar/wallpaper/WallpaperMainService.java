package eu.pintergabor.wstar.wallpaper;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.service.wallpaper.WallpaperService;
import android.view.SurfaceHolder;

import eu.pintergabor.wstar.ui.helper.Defaults;

public class WallpaperMainService extends WallpaperService {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public Engine onCreateEngine() {
        return new MainEngine();
    }

    private class MainEngine extends Engine {

        /**
         * This will do the actual work.
         */
        private final WallpaperDraw wallpaperDraw;

        /**
         * Set defaults
         */
        public MainEngine() {
            // Small optimization on new Android versions
            if (15 <= Build.VERSION.SDK_INT) {
                setOffsetNotificationsEnabled(false);
            }
            // Small optimization on new Android versions
            Context context;
            if (28 <= Build.VERSION.SDK_INT) {
                context = getDisplayContext();
            } else {
                context = getBaseContext();
            }
            // Set defaults
            Defaults.setDefaults(context);
            // Create the worker
            wallpaperDraw = new WallpaperDraw(context);
        }

        /**
         * Redraw WSTAR image.
         */
        @Override
        public void onSurfaceRedrawNeeded(SurfaceHolder holder) {
            super.onSurfaceRedrawNeeded(holder);
            draw();
        }

        /**
         * Needed, because after exit setup {@link #onSurfaceRedrawNeeded(SurfaceHolder)}
         * might not be called automatically.
         */
        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                draw();
            }
        }

        /**
         * Update parameters and redraw image.
         */
        private void draw() {
            wallpaperDraw.updateParams();
            final SurfaceHolder holder = getSurfaceHolder();
            Canvas canvas = null;
            try {
                canvas = holder.lockCanvas();
                if (canvas != null) {
                    wallpaperDraw.draw(canvas);
                }
            } finally {
                if (canvas != null) {
                    holder.unlockCanvasAndPost(canvas);
                }
            }
        }

    }
}
