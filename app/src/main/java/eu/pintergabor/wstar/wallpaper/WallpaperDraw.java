package eu.pintergabor.wstar.wallpaper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;

import androidx.annotation.*;
import androidx.preference.PreferenceManager;

import java.util.Map;

import eu.pintergabor.util.basic.Cast;
import eu.pintergabor.wstar.BuildConfig;
import eu.pintergabor.wstar.R;
import eu.pintergabor.wstar.drawings.NephroidDrawable;
import eu.pintergabor.wstar.drawings.PascalDrawable;
import eu.pintergabor.wstar.drawings.WDrawableBase;
import eu.pintergabor.wstar.drawings.WstarDrawable;

public class WallpaperDraw {

    // region // Constructor and the image.

    /**
     * Display context.
     */
    private final Context context;

    /**
     * The image.
     */
    private WDrawableBase wimage;

    /**
     * Store context
     *
     * @param context Display context.
     */
    public WallpaperDraw(Context context) {
        this.context = context;
    }

    // endregion

    // region // Utils.

    @NonNull
    @SuppressWarnings("SameParameterValue")
    private static String getMapString(@NonNull Map<String, ?> map, String key, String defValue) {
        final Object o = map.get(key);
        return Cast.ObjectToString(o, defValue);
    }

    @SuppressWarnings("SameParameterValue")
    private static int getMapInt(@NonNull Map<String, ?> map, String key, int defValue) {
        final Object o = map.get(key);
        return Cast.ObjectToInt(o, defValue);
    }

    @SuppressWarnings("SameParameterValue")
    private static float getMapFloat(@NonNull Map<String, ?> map, String key, float defValue) {
        final Object o = map.get(key);
        return Cast.ObjectToFloat(o, defValue);
    }

    // endregion

    // region // Update parameters and draw.

    /**
     * Update parameters from the saved preferences.
     */
    public void updateParams() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        final Map<String, ?> spmap = sp.getAll();
        int defaultpoints, defaultn, defaultm;
        final Resources r = context.getResources();
        // Recreate image if type is changed
        switch (getMapString(spmap, "wallpaper_drawing_type", BuildConfig.FLAVOR)) {
            default: // === case "wstar":
                if (!(wimage instanceof WstarDrawable)) {
                    wimage = new WstarDrawable();
                }
                defaultpoints = r.getInteger(R.integer.wstar_default_points);
                defaultn = r.getInteger(R.integer.wstar_default_n);
                defaultm = r.getInteger(R.integer.wstar_default_m);
                break;
            case "pascal":
                if (!(wimage instanceof PascalDrawable)) {
                    wimage = new PascalDrawable();
                }
                defaultpoints = r.getInteger(R.integer.pascal_default_points);
                defaultn = r.getInteger(R.integer.pascal_default_n);
                defaultm = r.getInteger(R.integer.pascal_default_m);
                break;
            case "nephroid":
                if (!(wimage instanceof NephroidDrawable)) {
                    wimage = new NephroidDrawable();
                }
                defaultpoints = r.getInteger(R.integer.nephroid_default_points);
                defaultn = r.getInteger(R.integer.nephroid_default_n);
                defaultm = r.getInteger(R.integer.nephroid_default_m);
                break;
        }
        // Update parameters
        wimage.setSize(getMapInt(spmap, "wallpaper_points", defaultpoints));
        wimage.setParamN(getMapInt(spmap, "wallpaper_param_n", defaultn));
        wimage.setParamM(getMapInt(spmap, "wallpaper_param_m", defaultm));
        wimage.setLineWidth(getMapFloat(spmap, "wallpaper_line_width", wimage.getLineWidth()));
        wimage.setBackColor(Color.rgb(
            getMapInt(spmap, "wallpaper_background_red", Color.red(wimage.getBackColor())),
            getMapInt(spmap, "wallpaper_background_green", Color.green(wimage.getBackColor())),
            getMapInt(spmap, "wallpaper_background_blue", Color.blue(wimage.getBackColor()))));
        wimage.setForeColor(Color.rgb(
            getMapInt(spmap, "wallpaper_foreground_red", Color.red(wimage.getForeColor())),
            getMapInt(spmap, "wallpaper_foreground_green", Color.green(wimage.getForeColor())),
            getMapInt(spmap, "wallpaper_foreground_blue", Color.blue(wimage.getForeColor()))));
    }

    /**
     * Draw the image.
     *
     * @param canvas Canvas to draw on.
     */
    public void draw(@NonNull Canvas canvas) {
        if (wimage != null) {
            wimage.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            wimage.draw(canvas);
        }
    }

    // endregion

}
