package eu.pintergabor.wstar.wallpaper;

import android.os.Bundle;

import eu.pintergabor.wstar.R;
import eu.pintergabor.wstar.ui.PrefsFragment;

/**
 * The core of wallpaper settings.
 */
public class WallpaperPrefsFragment extends PrefsFragment {

    /**
     * Inflate preferences and get reference to every preference.
     *
     * @param key Root key from {@link #onCreatePreferences(Bundle, String)}.
     */
    @Override
    protected void inflatePreferences(String key) {
        setPreferencesFromResource(R.xml.wallpaper_preferences, key);
        // Get references
        drawingType = findPreference("wallpaper_drawing_type");
        points = findPreference("wallpaper_points");
        paramN = findPreference("wallpaper_param_n");
        paramM = findPreference("wallpaper_param_m");
        lineWidth = findPreference("wallpaper_line_width");
        backgroundRed = findPreference("wallpaper_background_red");
        backgroundGreen = findPreference("wallpaper_background_green");
        backgroundBlue = findPreference("wallpaper_background_blue");
        foregroundRed = findPreference("wallpaper_foreground_red");
        foregroundGreen = findPreference("wallpaper_foreground_green");
        foregroundBlue = findPreference("wallpaper_foreground_blue");
    }

}
