package eu.pintergabor.wstar.wallpaper;

import android.os.Bundle;

import eu.pintergabor.wstar.R;
import eu.pintergabor.wstar.ui.BaseActivity;

/**
 * Main program
 */
public final class WallpaperSettingsActivity extends BaseActivity {

    /**
     * Start of the program.
     *
     * @param bundle Previous state.
     */
    @Override
    protected void onCreate(Bundle bundle) {
        // Restore state
        super.onCreate(bundle);
        // Main layout
        setContentView(R.layout.activity_wallpaper_settings);
    }

}
