package eu.pintergabor.wstar.file;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.*;
import androidx.core.app.ActivityCompat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import eu.pintergabor.util.file.PNG;
import eu.pintergabor.wstar.drawings.NephroidDrawable;
import eu.pintergabor.wstar.drawings.PascalDrawable;
import eu.pintergabor.wstar.drawings.WDrawableBase;
import eu.pintergabor.wstar.drawings.WstarDrawable;
import eu.pintergabor.wstar.main.MainHub;
import eu.pintergabor.wstar.ui.MainActivity;

public final class Save {

    // region // Constructor and link to MainActivity.

    /**
     * Link to {@link MainActivity}.
     */
    private final MainActivity mainActivity;


    /**
     * Nothing.
     */
    public Save(@NonNull MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    // endregion

    // region // Save.

    /**
     * Create a WSTAR drawable from the current parameters.
     */
    private WDrawableBase createImage() {
        WDrawableBase wimage;
        final MainHub mainHub = mainActivity.getMainHub();
        // Create image
        final String type = mainHub.getDrawingType();
        switch (type) {
            default: // === case "wstar":
                wimage = new WstarDrawable();
                break;
            case "pascal":
                wimage = new PascalDrawable();
                break;
            case "nephroid":
                wimage = new NephroidDrawable();
                break;
        }
        // Set parameters
        wimage.setSize(mainHub.getPoints());
        wimage.setParamN(mainHub.getParamN());
        wimage.setParamM(mainHub.getParamM());
        wimage.setLineWidth(mainHub.getLineWidth());
        wimage.setBackColor(mainHub.getBackColor());
        wimage.setForeColor(mainHub.getForeColor());
        // Return
        return wimage;
    }

    private int saveWidth;
    private int saveHeight;

    /**
     * Create a bitmap, drawn a WSTAR on it, and save it.
     */
    private void saveBitmap() {
        // Create bitmap
        Bitmap bitmap;
        if (0 < saveWidth && 0 < saveHeight) {
            bitmap = Bitmap.createBitmap(saveWidth, saveHeight, Bitmap.Config.ARGB_8888);
        } else {
            return;
        }
        // Create WSTAR
        WDrawableBase wimage = createImage();
        // Draw WSTAR on bitmap
        wimage.setBounds(0, 0, saveWidth, saveHeight);
        Canvas canvas = new Canvas(bitmap);
        wimage.draw(canvas);
        // Save bitmap
        DateFormat df = new SimpleDateFormat(" yyyy-MM-dd HH.mm.ss.SSS", Locale.getDefault());
        String date = df.format(Calendar.getInstance().getTime());
        final MainHub mainHub = mainActivity.getMainHub();
        String basename = mainHub.getDrawingType();
        String name = basename + date;
        PNG.saveImage(mainActivity, bitmap, name);
        // Toast
        final Toast toast = Toast.makeText(mainActivity, name + " saved.", Toast.LENGTH_LONG);
        toast.show();
        // Done
        saveWidth = 0;
        saveHeight = 0;
    }

    /**
     * WRITE_EXTERNAL_STORAGE request code.
     */
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST = 1;

    /**
     * Save WSTAR.
     *
     * @param width  Width in pixels.
     * @param height Height in pixels.
     */
    public void save(int width, int height) {
        saveWidth = width;
        saveHeight = height;
        // Check permission
        if (ActivityCompat.checkSelfPermission(mainActivity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
            PackageManager.PERMISSION_GRANTED) {
            // Save now
            saveBitmap();
        } else {
            // Ask for permission and save later
            ActivityCompat.requestPermissions(mainActivity,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                WRITE_EXTERNAL_STORAGE_REQUEST);
        }
    }

    public void onRequestPermissionsResult(
        int requestCode,
        @NonNull @SuppressWarnings("unused") String[] permissions,
        @NonNull int[] grantResults) {
        if (requestCode == WRITE_EXTERNAL_STORAGE_REQUEST &&
            grantResults.length > 0 &&
            grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Got permission, can save bitmap now
            saveBitmap();
        }
    }

    /**
     * Called from MainActivity.onSaveInstanceState.
     * <p>
     * Save {@link #saveWidth} and {@link #saveHeight}.
     */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt("save_width", saveWidth);
        bundle.putInt("save_height", saveHeight);
    }

    /**
     * Called from MainActivity.onRestoreInstanceState or from MainActivity.onCreate.
     * <p>
     * Restore {@link #saveWidth} and {@link #saveHeight}.
     */
    public void onRestoreInstanceState(Bundle bundle) {
        if (bundle != null) {
            saveWidth = bundle.getInt("save_width", 0);
            saveHeight = bundle.getInt("save_height", 0);
        }
    }

    // endregion

}
