package eu.pintergabor.util.ui;

import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Detect scrolls.
 */
@SuppressWarnings("unused")
public class ScrollDetector extends GestureDetector.SimpleOnGestureListener {

    /**
     * Every gesture starts with a touch down.
     *
     * @param event Unused.
     * @return true.
     */
    @Override
    public boolean onDown(MotionEvent event) {
        return true;
    }

    /**
     * All scroll gesture events.
     *
     * @param event1    Start.
     * @param event2    End.
     * @param distanceX Unused.
     * @param distanceY Unused.
     * @return true.
     */
    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2,
                            float distanceX, float distanceY) {
        final float startX = event1.getX();
        final float startY = event1.getY();
        if (scrollListener != null) {
            scrollListener.onScroll(event1.getX(), event1.getY(),
                distanceX, distanceY);
        }
        return true;
    }

    // region // Scroll

    private ScrollListener scrollListener;

    /**
     * Set callback on detected scroll.
     *
     * @param listener Callback.
     */
    public void setScrollListener(ScrollListener listener) {
        scrollListener = listener;
    }

    /**
     * Callback on scroll.
     */
    public interface ScrollListener {
        /**
         * Called at the end of the gesture.
         *
         * @param startX    Scroll start X.
         * @param startY    Scroll start Y.
         * @param distanceX Scroll distance X.
         * @param distanceY Scroll distance Y.
         */
        void onScroll(float startX, float startY,
                      float distanceX, float distanceY);
    }

    // endregion

}
