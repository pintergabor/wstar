package eu.pintergabor.util.ui;

import android.view.ScaleGestureDetector;

/**
 * Detect two finger gestures, like pan and scale.
 */
@SuppressWarnings("unused")
public class ZoomDetector extends ScaleGestureDetector.SimpleOnScaleGestureListener {

    /**
     * Follow the fingers.
     * <p>
     * Track gesture focus point.
     *
     * @param detector To get more info.
     * @return true.
     */
    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        float x = detector.getFocusX();
        float y = detector.getFocusX();
        float scale = detector.getScaleFactor();
        // Fire up zoom events
        if (zoomListener != null) {
            zoomListener.onZoom(x, y, scale);
        }
        return true;
    }

    // region // Zoom

    private ZoomListener zoomListener;

    /**
     * Set callback on detected zoom.
     *
     * @param listener Callback.
     */
    public void setZoomListener(ZoomListener listener) {
        zoomListener = listener;
    }

    /**
     * Callback on zoom.
     */
    public interface ZoomListener {
        /**
         * Called at the end of the two finger gesture.
         *
         * @param startX Pan/zoom start X.
         * @param startY Pan/zoom start Y.
         * @param scale  Total span change.
         */
        void onZoom(float startX, float startY, float scale);
    }

    // endregion

}
