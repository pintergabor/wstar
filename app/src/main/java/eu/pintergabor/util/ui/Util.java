package eu.pintergabor.util.ui;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.*;

/**
 * Static class, containing simple, common functions.
 */
public final class Util {

    /**
     * Static class.
     */
    private Util() {
        // Nothing.
    }

    /**
     * Set width of a {@link View}.
     *
     * @param view   The {@link View}.
     * @param width  Desired width in pixels.
     * @param height Desired height in pixels.
     */
    public static void setSize(@NonNull View view, int width, int height) {
        final ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = width;
        params.height = height;
        view.setLayoutParams(params);
    }

    /**
     * Set offset of a {@link View}.
     *
     * @param view   The {@link View}.
     * @param offset Desired offset in pixels.
     */
    public static void setOffset(@NonNull View view, int offset) {
        final ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        params.leftMargin = offset;
        params.rightMargin = -offset;
        view.setLayoutParams(params);
    }

}
