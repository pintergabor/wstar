package eu.pintergabor.util.preferences;

import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceManager;

/**
 * Update the summary when the value changes, per the Android Design guidelines.
 */
public class ValueToSummary implements Preference.OnPreferenceChangeListener {

    /**
     * Updates the preference's summary to reflect its new value.
     *
     * @return true
     */
    @SuppressWarnings("unused")
    public static boolean updateSummary(Preference preference, Object value) {
        if (value != null) {
            final String stringValue = value.toString();
            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // 'entries' list of the preference.
                final ListPreference listPreference = (ListPreference) preference;
                final int index = listPreference.findIndexOfValue(stringValue);
                // Set the summary to reflect the new value.
                if (0 <= index) {
                    preference.setSummary(listPreference.getEntries()[index]);
                }
            } else {
                // For all other preferences, set the summary to the
                // simple string representation of the value.
                preference.setSummary(stringValue);
            }
        }
        return true;
    }

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     *
     * @return true
     */
    @Override
    public boolean onPreferenceChange(Preference preference, Object value) {
        return updateSummary(preference, value);
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #onPreferenceChange(Preference, Object)
     */
    @SuppressWarnings("unused")
    public void bindPreferenceValueToSummary(Preference preference) {
        if (preference != null) {
            // Set the listener to watch for value changes.
            preference.setOnPreferenceChangeListener(this);
            // Trigger the listener immediately with the current value of the preference.
            onPreferenceChange(preference,
                PreferenceManager
                    .getDefaultSharedPreferences(preference.getContext())
                    .getString(preference.getKey(), ""));
        }
    }

}
