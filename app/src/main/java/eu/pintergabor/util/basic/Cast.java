package eu.pintergabor.util.basic;

import androidx.annotation.Nullable;

/**
 * Utilities for casting something to something else, without throwing exception.
 */
public final class Cast {

    /**
     * It is a static class.
     */
    private Cast() {
        // Nothing.
    }

    // region // String

    /**
     * Convert {@link Object} to String, without throwing exception.
     *
     * @param o        {@link Object} to convert.
     * @param defValue Default value, if {@code o} is invalid.
     * @return String representation of {@code o}.
     */
    @SuppressWarnings("unused")
    public static String ObjectToString(@Nullable Object o, String defValue) {
        if (o != null) {
            if (o instanceof String) {
                return (String) o;
            }
        }
        return defValue;
    }

    /**
     * Convert {@link Object} to String, without throwing exception.
     *
     * @param o {@link Object} to convert.
     * @return String representation of {@code o}.
     */
    @SuppressWarnings("unused")
    public static String ObjectToString(@Nullable Object o) {
        return ObjectToString(o, "");
    }

    // endregion // String

    // region // boolean

    /**
     * Convert {@link Object} to boolean, without throwing exception.
     *
     * @param o        {@link Object} to convert.
     * @param defValue Default value, if {@code o} is invalid.
     * @return Boolean representation of {@code o}.
     */
    @SuppressWarnings("unused")
    public static boolean ObjectToBoolean(@Nullable Object o, boolean defValue) {
        if (o != null) {
            if (o instanceof Boolean) {
                return (boolean) o;
            }
            if (o instanceof Integer) {
                return (int) o != 0;
            }
            if (o instanceof Double) {
                return (double) o != 0d;
            }
            if (o instanceof Float) {
                return (float) o != 0f;
            }
            if (o instanceof String) {
                try {
                    return Boolean.parseBoolean((String) o);
                } catch (NumberFormatException e) {
                    // return defValue;
                }
            }
        }
        return defValue;
    }

    /**
     * Convert {@link Object} to boolean, without throwing exception.
     *
     * @param o {@link Object} to convert.
     * @return Boolean representation of {@code o}.
     */
    @SuppressWarnings("unused")
    public static boolean ObjectToBoolean(@Nullable Object o) {
        return ObjectToBoolean(o, false);
    }

    // endregion // boolean

    // region // int

    /**
     * Convert {@link Object} to int, without throwing exception.
     *
     * @param o        {@link Object} to convert.
     * @param defValue Default value, if {@code o} is invalid.
     * @return Integer representation of {@code o}.
     */
    @SuppressWarnings("unused")
    public static int ObjectToInt(@Nullable Object o, int defValue) {
        if (o != null) {
            if (o instanceof Integer) {
                return (int) o;
            }
            if (o instanceof Double) {
                return (int) Math.round((double) o);
            }
            if (o instanceof Float) {
                return Math.round((float) o);
            }
            if (o instanceof String) {
                try {
                    return Integer.parseInt((String) o);
                } catch (NumberFormatException e) {
                    // return defValue;
                }
            }
        }
        return defValue;
    }

    /**
     * Convert {@link Object} to int, without throwing exception.
     *
     * @param o {@link Object} to convert.
     * @return Integer representation of {@code o}.
     */
    @SuppressWarnings("unused")
    public static int ObjectToInt(@Nullable Object o) {
        return ObjectToInt(o, 0);
    }

    // endregion // int

    // region // float

    /**
     * Convert {@link Object} to float, without throwing exception.
     *
     * @param o        {@link Object} to convert.
     * @param defValue Default value, if {@code o} is invalid.
     * @return Float representation of {@code o}.
     */
    @SuppressWarnings("unused")
    public static float ObjectToFloat(@Nullable Object o, float defValue) {
        if (o != null) {
            if (o instanceof Integer) {
                return (float) (int) o;
            }
            if (o instanceof Double) {
                return (float) (double) o;
            }
            if (o instanceof Float) {
                return (float) o;
            }
            if (o instanceof String) {
                try {
                    return Float.parseFloat((String) o);
                } catch (NumberFormatException e) {
                    // return defValue;
                }
            }
        }
        return defValue;
    }

    /**
     * Convert {@link Object} to float, without throwing exception.
     *
     * @param o {@link Object} to convert.
     * @return Float representation of {@code o}.
     */
    @SuppressWarnings("unused")
    public static float ObjectToFloat(@Nullable Object o) {
        return ObjectToFloat(o, 0);
    }

    // endregion // float

    // region // double

    /**
     * Convert {@link Object} to double, without throwing exception.
     *
     * @param o        {@link Object} to convert.
     * @param defValue Default value, if {@code o} is invalid.
     * @return Double representation of {@code o}.
     */
    @SuppressWarnings("unused")
    public static double ObjectToDouble(@Nullable Object o, float defValue) {
        if (o != null) {
            if (o instanceof Integer) {
                return (double) (int) o;
            }
            if (o instanceof Double) {
                return (double) o;
            }
            if (o instanceof Float) {
                return (double) (float) o;
            }
            if (o instanceof String) {
                try {
                    return Double.parseDouble((String) o);
                } catch (NumberFormatException e) {
                    // return defValue;
                }
            }
        }
        return defValue;
    }

    /**
     * Convert {@link Object} to double, without throwing exception.
     *
     * @param o {@link Object} to convert.
     * @return Double representation of {@code o}.
     */
    @SuppressWarnings("unused")
    public static double ObjectToDouble(@Nullable Object o) {
        return ObjectToDouble(o, 0);
    }

    // endregion // double
}
