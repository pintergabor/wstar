package eu.pintergabor.util.runcount;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

@SuppressWarnings("unused")
public class RunCount {

    // region // Eager singleton pattern.

    private static RunCount instance = new RunCount();

    private RunCount() {
        // Nothing.
    }

    /**
     * Get instance.
     *
     * @return Singleton.
     */
    @SuppressWarnings("unused")
    public static RunCount getInstance() {
        return instance;
    }

    // endregion

    /**
     * See {@link #get()}
     */
    private int runCount;

    /**
     * Run counter.
     */
    @SuppressWarnings("unused")
    public int get() {
        return runCount;
    }

    /**
     * Increment run counter. See {@link #get()}
     *
     * @return The incremented value.
     */
    @SuppressWarnings({"unused", "UnusedReturnValue"})
    public int increment() {
        runCount = runCount + 1;
        return runCount;
    }

    /**
     * See {@link #get()}
     * <p>
     * Rarely used.
     */
    @SuppressWarnings("unused")
    public void set(int runCount) {
        this.runCount = runCount;
    }

    /**
     * Load, and increment, run counter from preferences.
     * <p>
     * Called from onCreate of MainActivity.
     *
     * @param context Usually MainActivity.
     * @return Instance, to make writing load and increment easy.
     */
    @SuppressWarnings("UnusedReturnValue")
    public RunCount load(Context context) {
        // Run count is stored among the preferences, for convenience.
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        // Load
        try {
            runCount = prefs.getInt("run_count", 0);
        } catch (ClassCastException e) {
            runCount = 0;
        }
        return instance;
    }

    /**
     * Save run counter among preferences.
     * <p>
     * Called from onPause of MainActivity.
     *
     * @param context Usually MainActivity.
     */
    public void save(Context context) {
        // Run count is stored among the preferences, for convenience.
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor prefed = prefs.edit();
        // Store
        prefed.putInt("run_count", runCount);
        prefed.apply();
    }

}
