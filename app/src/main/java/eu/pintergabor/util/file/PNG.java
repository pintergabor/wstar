package eu.pintergabor.util.file;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@SuppressWarnings("unused")
public final class PNG {

    /**
     * Static class
     */
    private PNG() {
        // Nothing.
    }

    /**
     * Save bitmap to file in png format.
     *
     * @param context Some context.
     * @param bitmap  The bitmap to save.
     * @param name    File name without ".png" extension.
     * @return true on success.
     */
    @SuppressWarnings("UnusedReturnValue")
    public static boolean saveImage(Context context, Bitmap bitmap, @NonNull String name) {
        boolean result = true;
        OutputStream stream = null;
        try {
            if (29 <= Build.VERSION.SDK_INT) {
                // Create header.
                final ContentResolver resolver = context.getContentResolver();
                final ContentValues contentValues = new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/png");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES);
                // Try external storage first,
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                // then fall back to internal storage.
                if (imageUri == null) {
                    imageUri = resolver.insert(MediaStore.Images.Media.INTERNAL_CONTENT_URI, contentValues);
                }
                // Open file for writing.
                if (imageUri != null) {
                    stream = resolver.openOutputStream(imageUri);
                }
            } else {
                // Path.
                final String imagesDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES).toString();
                // If the directory does not exist, try to create it.
                final File dir = new File(imagesDir);
                if (!dir.exists()) {
                    if (!dir.mkdirs()) {
                        result = false;
                    }
                }
                // Create and open file.
                final File file = new File(imagesDir, name + ".png");
                stream = new FileOutputStream(file);
            }
            // Write
            if (result && stream != null) {
                result = bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                stream.flush();
                stream.close();
            }
        } catch (IOException e1) {
            // Failed.
            result = false;
        }
        // Done
        return result;
    }

}
