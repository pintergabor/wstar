package eu.pintergabor.test;

import android.util.Log;

import androidx.annotation.NonNull;

import java.util.Locale;

import eu.pintergabor.wstar.drawings.CircleArrayOfPoints;

@SuppressWarnings("unused")
public class StaticTests {


    /**
     * Draw a line.
     *
     * @param x0 Start X.
     * @param y0 Start Y.
     * @param x1 End X.
     * @param y1 End Y.
     * @return a line in SVG.
     */
    private static String line(float x0, float y0, float x1, float y1) {
        return String.format(Locale.ROOT,
            "M%.1f,%.1fL%.1f,%.1f",
            x0, y0, x1, y1);
    }

    /**
     * Draw a circle.
     *
     * @param x Center X.
     * @param y Center Y.
     * @param r Radius.
     * @return a line in SVG.
     */
    @SuppressWarnings("unused")
    @NonNull
    private static String circle(float x, float y, float r) {
        return String.format(Locale.ROOT,
            "M%.1f,%.1fa%.1f,%.1f,0,1,0,%.1f,0a%.1f,%.1f,0,1,0,%.1f,0",
            x - r, y, r, r, 2 * r, r, r, -2 * r);
    }

    /**
     * int / float conversion multiplier.
     * <p>
     * 0.1f means "be accurate to 1 decimal place"
     */
    private static final float MULT = 10f;

    @SuppressWarnings("SameParameterValue")
    private static String drawWSTAR(int size, int centerx, int centery, int radius) {
        CircleArrayOfPoints points =
            new CircleArrayOfPoints(size, MULT * centerx, MULT * centery, MULT * radius);
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < points.getSize() - 1; i++) {
            for (int j = i + 1; j < points.getSize(); j++) {
                s.append(line(
                    points.getX(i) / MULT, points.getY(i) / MULT,
                    points.getX(j) / MULT, points.getY(j) / MULT
                ));
            }
        }
        s.append("Z");
        return s.toString();
    }

    @SuppressWarnings("SameParameterValue")
    private static String drawPascal(int size, int centerx, int centery, int radius) {
        CircleArrayOfPoints points =
            new CircleArrayOfPoints(size, MULT * centerx, MULT * centery, MULT * radius);
        StringBuilder s = new StringBuilder();
        int i = 0, j = 0;
        do {
            j = (j + 2) % size;
            i = (i + 1) % size;
            s.append(line(
                points.getX(i) / MULT, points.getY(i) / MULT,
                points.getX(j) / MULT, points.getY(j) / MULT
            ));
        } while (i != 0);
        return s.toString();
    }

    @SuppressWarnings("SameParameterValue")
    private static String drawNephroid(int size, int centerx, int centery, int radius) {
        CircleArrayOfPoints points =
            new CircleArrayOfPoints(size, MULT * centerx, MULT * centery, MULT * radius);
        StringBuilder s = new StringBuilder();
        for (int i = 1; i < size; i++) {
            s.append(circle(
                points.getX(i) / MULT, points.getY(i) / MULT,
                Math.abs(points.getX(i) - MULT * centerx) / MULT
            ));
        }
        return s.toString();
    }

    /**
     * Make a launcher icons and menu icons.
     * <p>
     * All sizes are in dp.
     * <p>
     * Call at the end of MainActivity.onCreate.
     */
    public static void makeLauncherIcon() {
        Log.d("LAUNCHERICON WSTAR", drawWSTAR(11, 54, 54, 30));
        Log.d("LAUNCHERICON PASCAL", drawPascal(100, 54, 54, 30));
        Log.d("LAUNCHERICON NEPHROID", drawNephroid(20, 54, 54, 15));
        Log.d("MENUICON WSTAR", drawWSTAR(7, 12, 12, 12));
        Log.d("MENUICON PASCAL", drawPascal(50, 12, 12, 12));
        Log.d("MENUICON NEPHROID", drawNephroid(10, 12, 12, 6));
    }

}
